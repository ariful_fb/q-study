<?php
/**
 * Module controller class
 */
class Module extends CI_Controller
{

    public $loggedUserId, $loggedUserType;


    public function __construct()
    {
        parent::__construct();

        $user_id              = $this->session->userdata('user_id');
        $user_type            = $this->session->userdata('userType');
        $this->loggedUserId   = $user_id;
        $this->loggedUserType = $user_type;

        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }

        $this->load->model('Parent_model');
        $this->load->model('tutor_model');
        $this->load->model('Student_model');
        $this->load->model('ModuleModel');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->model('Preview_model');
        $this->load->model('QuestionModel');
        $this->load->helper('CommonMethods');

    }//end __construct()


    public function view_course()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $data['maincontent'] = $this->load->view('module/view_course', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end view_course()


    public function all_module()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $user_id = $this->session->userdata('user_id');

        $data['user_info']          = $this->tutor_model->userInfo($user_id);
        $data['all_module']         = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_grade']          = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type']    = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course']         = $this->tutor_model->getAllInfo('tbl_course');
        $data['all_subject']        = $this->tutor_model->getAllInfo('tbl_subject');
        $data['allRenderedModType'] = $this->renderAllModuleType();
        $data['all_country']        = $this->renderAllCountry();

        $studentIds          = $this->tutor_model->allStudents(['sct_id' => $user_id]);
        $data['allStudents'] = $this->renderStudentIds($studentIds);

        $data['maincontent'] = $this->load->view('module/all_module', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end all_module()


    /**
     * Add module (view part)
     *
     * @return void
     */
    public function add_module()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $user_id            = $this->session->userdata('user_id');

        $data['user_info']         = $this->tutor_model->userInfo($user_id);
        $data['all_module']        = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_module_type']   = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course']        = $this->tutor_model->getAllInfo('tbl_course');
        $data['all_country']       = $this->renderAllCountry();
        $data['all_subjects']      = $this->renderAllSubject();
        $data['all_chapters']      = $this->renderAllChapter();
        $data['all_module_type']   = $this->renderAllModuleType();
        $data['all_question_type'] = $this->tutor_model->getAllInfo('tbl_questiontype');
        foreach ($data['all_question_type'] as $row) {
            $question_list[$row['id']] = $this->tutor_model->getUserQuestion('tbl_question', $row['id'], $user_id);
        }

        $data['all_question'] = $question_list;
        $studentIds           = $this->tutor_model->allStudents(['sct_id' => $user_id]);
        $data['allStudents']  = $this->renderStudentIds($studentIds);

        $data['maincontent'] = $this->load->view('module/add_module', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end add_module()


    /**
     * Responsible for saving module data.
     *
     * @return void
     */
    public function saveModuleQuestion()
    {
        $post              = $this->input->post();
        $clean             = $this->security->xss_clean($post);
        $moduleTableData   = [];
        $moduleTableData[] = [
            'moduleName'        => $clean['moduleName'],
            'trackerName'       => $clean['trackerName'],
            'individualName'    => $clean['individualName'],
            'isSMS'             => isset($clean['isSMS']) ? $clean['isSMS'] : 0,
            'isAllStudent'      => isset($clean['isAllStudent']) ? $clean['isAllStudent'] : 0,
            'individualStudent' => isset($clean['individualStudent']) ? json_encode($clean['individualStudent']) : '',
            'subject'           => $clean['subject'],
            'chapter'           => $clean['chapter'],
            'country'           => $clean['country'],
            'studentGrade'      => $clean['studentGrade'],
            'moduleType'        => $clean['moduleType'],
            'user_id'           => $this->loggedUserId,
            'user_type'         => $this->loggedUserType,
            'exam_date'         => isset($clean['dateCreated']) ? strtotime($clean['dateCreated']) : 0,
            'exam_start'        => isset($clean['startTime']) ? strtotime($clean['startTime']) : 0,
            'exam_end'          => isset($clean['endTime']) ? strtotime($clean['endTime']) : 0,
            'optionalTime'      => isset($clean['optTime']) ? strtotime($clean['optTime']) : 0,
        ];
        // Save module info first
        $moduleId = $this->ModuleModel->insert('tbl_module', $moduleTableData);

        // If ques order set record those to tbl_modulequestion table
        $arr   = [];
        $items = isset($clean['qId_ordr']) ? array_filter($clean['qId_ordr']) : [];
        if (count($items)) {
            foreach ($items as $qId_ordr) {
                $temp  = explode('_', $qId_ordr);
                $arr[] = [
                    'question_id'    => $temp[0],
                    'module_id'      => $moduleId,
                    'question_order' => $temp[1],
                    'created'        => time(),
                ];
            }

            $this->ModuleModel->insert('tbl_modulequestion', $arr);
        }

        //Save individual student/all student ids on tbl_module_student table
        $dataToInsert = [];
        if(isset($clean['isAllStudent'])){
            $allStudentIds = $this->Student_model->allStudents(['sct_id' => $this->loggedUserId]);
            
        } else if (isset($clean['individualStudent'])){
             $allStudentIds = $clean['individualStudent'];
        }
        foreach ($allStudentIds as $studentId) {
                $dataToInsert[] = array(
                    'module_id'  => $moduleId,
                    'student_id' => $studentId,
                );
            }
        $this->ModuleModel->insert('tbl_module_student', $dataToInsert);


        if ($moduleId) {
            echo 'true';
            // Module recorded.
        } else {
            echo 'false';
            // Module record failed.
        }

        // $this->session->set_flashdata('success_msg', 'Module Saved Successfully.');
        // redirect('all-module');

    }//end saveModuleQuestion()


    /**
     * This method will duplicate  a module with additional info given
     *
     * @return void
     */
    public function moduleDuplicate()
    {
        $post   = $this->input->post();
        $newMod = $this->security->xss_clean($post);

        $origModId  = $newMod['origModId'];
        $origMod    = $this->ModuleModel->moduleInfo($origModId);
        $newModName = isset($newMod['moduleName']) ? $newMod['moduleName'] : '';
        if ($newModName == $origMod['moduleName'] && $origMod['country'] == $newMod['country'] && $origMod['studentGrade'] == $newMod['studentGrade']) {
                echo 'false';
            die;
                // return 0;
        } else {
            $moduleTableData   = [];
            $moduleTableData[] = [
                'moduleName'        => $newMod['moduleName'],
                'trackerName'       => $origMod['trackerName'],
                'individualName'    => $origMod['individualName'],
                'isSMS'             => isset($newMod['isSMS']) ? $newMod['isSMS'] : 0,
                'isAllStudent'      => isset($newMod['isAllStudent']) ? $newMod['isAllStudent'] : 0,
                'individualStudent' => isset($newMod['individualStudent']) ? json_encode($newMod['individualStudent']) : $origMod['individualStudent'],
                'subject'           => $origMod['subject'],
                'chapter'           => $origMod['chapter'],
                'country'           => $newMod['country'],
                'studentGrade'      => $newMod['studentGrade'],
                'moduleType'        => $newMod['moduleType'],
                'user_id'           => $this->loggedUserId,
                'user_type'         => $this->loggedUserType,
                'exam_date'         => isset($newMod['dateCreated']) ? strtotime($newMod['dateCreated']) : time(),
            ];
            // Save module info first
            $newModuleId = $this->ModuleModel->insert('tbl_module', $moduleTableData);
            $origModQues = $this->ModuleModel->moduleQuestion($origModId);
            $arr         = [];
            if (count($origModQues)) {
                foreach ($origModQues as $ques) {
                    $arr[] = [
                        'question_id'    => $ques['question_id'],
                        'module_id'      => $newModuleId,
                        'question_order' => $ques['question_order'],
                        'created'        => time(),
                    ];
                }

                $this->ModuleModel->insert('tbl_modulequestion', $arr);
            }

            echo 'true';
        }//end if

    }//end moduleDuplicate()


    public function editModule($moduleId)
    {
        $module = $this->ModuleModel->moduleInfo($moduleId);
        if (!sizeof($module)) {
            $this->session->set_flashdata('error_msg', 'Module not exists.');
            redirect('all-module');
        }

        $moduleQuestion = $this->ModuleModel->moduleQuestion($moduleId);
        $quesOrdrMap    = [];
        foreach ($moduleQuestion as $temp) {
            $quesOrdrMap[$temp['question_id']] = $temp['question_order'];
        }

        $data['qoMap']      = $quesOrdrMap;
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        $user_id            = $this->session->userdata('user_id');

        $data['module_info']       = $module;
        $data['all_country']       = $this->renderAllCountry($module['country']);
        $data['all_subjects']      = $this->renderAllSubject($module['subject']);
        $data['all_chapters']      = $this->renderAllChapter($module['chapter']);
        $data['all_module_type']   = $this->renderAllModuleType($module['moduleType']);
        $data['all_question_type'] = $this->tutor_model->getAllInfo('tbl_questiontype');

        foreach ($data['all_question_type'] as $row) {
            $question_list[$row['id']] = $this->tutor_model->getUserQuestion('tbl_question', $row['id'], $user_id);
        }

        $indivStdIds          = $module['individualStudent'];
        $data['all_question'] = $question_list;
        $studentIds           = $this->tutor_model->allStudents(['sct_id' => $user_id]);
        $data['allStudents']  = $this->renderStudentIds($studentIds, $indivStdIds);

        $data['maincontent'] = $this->load->view('module/edit_module', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end editModule()


    public function updateRequestedModule()
    {
        $post              = $this->input->post();
        $clean             = $this->security->xss_clean($post);
        $moduleToUpdate    = $clean['moduleId'];
        $moduleTableData   = [];
        $moduleTableData[] = [
            'id'                => $moduleToUpdate,
            'moduleName'        => $clean['moduleName'],
            'trackerName'       => $clean['trackerName'],
            'individualName'    => $clean['individualName'],
            'isSMS'             => isset($clean['isSMS']) ? $clean['isSMS'] : 0,
            'isAllStudent'      => isset($clean['isAllStudent']) ? $clean['isAllStudent'] : 0,
            'individualStudent' => isset($clean['individualStudent']) ? json_encode($clean['individualStudent']) : '',
            'subject'           => $clean['subject'],
            'chapter'           => $clean['chapter'],
            'country'           => $clean['country'],
            'studentGrade'      => $clean['studentGrade'],
            'moduleType'        => $clean['moduleType'],
            'user_id'           => $this->loggedUserId,
            'user_type'         => $this->loggedUserType,
            'exam_date'         => isset($clean['dateCreated']) ? strtotime($clean['dateCreated']) : time(),
        ];

        // Update module info first
        $this->ModuleModel->update('tbl_module', $moduleTableData, 'id');

        // If ques order set, delete recorded module_question first,
        // then insert requested data to tbl_modulequestion table
        $arr   = [];
        $items = isset($clean['qId_ordr']) ? array_filter($clean['qId_ordr']) : [];
        if (count($items)) {
            $this->ModuleModel->deleteModuleQuestion($moduleToUpdate);
            foreach ($items as $qId_ordr) {
                $temp  = explode('_', $qId_ordr);
                $arr[] = [
                    'question_id'    => $temp[0],
                    'module_id'      => $moduleToUpdate,
                    'question_order' => $temp[1],
                    'created'        => time(),
                ];
            }

            $this->ModuleModel->insert('tbl_modulequestion', $arr);
        }

        echo 'true';

    }//end updateRequestedModule()


    /**
     * Module reorder view part
     *
     * @return void
     */
    public function reorderModule()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $user_id                    = $this->session->userdata('user_id');
        $data['user_info']          = $this->tutor_model->userInfo($user_id);
        $data['all_module']         = $this->ModuleModel->allModule();
        $data['all_grade']          = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type']    = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course']         = $this->tutor_model->getAllInfo('tbl_course');
        $data['allRenderedModType'] = $this->renderAllModuleType();
        $data['all_country']        = $this->renderAllCountry();
        $data['row']                = $this->renderReorderPageModule($data['all_module']);
        $studentIds                 = $this->tutor_model->allStudents(['sct_id' => $user_id]);
        $data['allStudents']        = $this->renderStudentIds($studentIds);

        $data['maincontent'] = $this->load->view('module/reorder_module', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end reorderModule()


    /**
     * Module order save(save on ajax call)
     *
     * @return string
     */
    public function saveModuleOrdering()
    {
        $post  = $this->input->post();
        $clean = $this->security->xss_clean($post);

        $arr   = [];
        $items = isset($clean['modId_ordr']) ? array_filter($clean['modId_ordr']) : [];

        if (count($items)) {
            foreach ($items as $modId_ordr) {
                $temp  = explode('_', $modId_ordr);
                $arr[] = [
                    'id'         => $temp[0],
                    'ordering'   => $temp[1],
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }

            $this->ModuleModel->update('tbl_module', $arr, 'id');
            echo 'true';
        }

    }//end saveModuleOrdering()


    /**
     * Wrap all students name with option tag.
     *
     * @return string Students.
     */
    public function renderStudentIds($studentIds, $selectedIds='')
    {
        $sel    = [];
        $stdIds = [];
        if (strlen($selectedIds) > 1) {
            $stdIds = json_decode($selectedIds);
        }

        $option  = '';
        $option .= '<option value="">--Student--</option>';
        foreach ($studentIds as $studentId) {
            $stInfo  = $this->Student_model->getInfo('tbl_useraccount', 'id', $studentId);
            $sel     = in_array($studentId, $stdIds) ? 'selected' : '';
            $option .= '<option value="'.$studentId.'" '.$sel.'>'.$stInfo[0]['name'].'</option>';
        }

        return $option;

    }//end renderStudentIds()


    /**
     * Wrap all Countries recorded in DB with option tag.
     *
     * @return string Countries.
     */
    public function renderAllCountry($selectedId=-1)
    {
        $option    = '';
        $option   .= '<option value="">--Country--</option>';
        $countries = $this->tutor_model->getAllInfo('tbl_country');
        foreach ($countries as $country) {
            $sel     = ($country['id'] == $selectedId) ? 'selected' : '';
            $option .= '<option value="'.$country['id'].'" '.$sel.'>'.$country['countryName'].'</option>';
        }

        return $option;

    }//end renderAllCountry()


    /**
     * Wrap all Subjects with option tag.
     *
     * @return string Users created subjects.
     */
    public function renderAllSubject($selectedId=-1)
    {
        $option   = '';
        $option  .= '<option value="">--Subject--</option>';
        $subjects = $this->tutor_model->getInfo('tbl_subject', 'created_by', $this->loggedUserId);
        foreach ($subjects as $subject) {
            $sel     = ($subject['subject_id'] == $selectedId) ? 'selected' : '';
            $option .= '<option value="'.$subject['subject_id'].'" '.$sel.'>'.$subject['subject_name'].'</option>';
        }

        return $option;

    }//end renderAllSubject()


    /**
     * Wrap all chapters with option tag.
     *
     * @return string Users created chapters.
     */
    public function renderAllChapter($selectedId=-1)
    {
        $option   = '';
        $option  .= '<option value="">--Chapter--</option>';
        $chapters = $this->tutor_model->getInfo('tbl_chapter', 'created_by', $this->loggedUserId);

        foreach ($chapters as $chapter) {
            $sel     = ($chapter['id'] == $selectedId) ? 'selected' : '';
            $option .= '<option value="'.$chapter['id'].'" '.$sel.'>'.$chapter['chapterName'].'</option>';
        }

        return $option;

    }//end renderAllChapter()


    /**
     * Wrap all Module types with option tag.
     *
     * @return string All module types recorded in database.
     */
    public function renderAllModuleType($selectedId=-1)
    {
        $option      = '';
        $option     .= '<option value="">--Moduletype--</option>';
        $moduleTypes = $this->ModuleModel->allModuleType();

        foreach ($moduleTypes as $moduleType) {
            $sel     = ($moduleType['id'] == $selectedId) ? 'selected' : '';
            $option .= '<option value="'.$moduleType['id'].'" '.$sel.'>'.$moduleType['module_type'].'</option>';
        }

        return $option;

    }//end renderAllModuleType()

    public function renderAllModule($modules)
    {
        $row = '';
        foreach($modules as $module){
            $row .= '<tr id="'.$module['id'].'">';
            $row .= '<td>'.date('d-M-Y', $module['exam_date']).'</td>';
            $row .= '<td id="modName">'.$module['moduleName'].'</td>';
            $row .= '<td>Everyday Study</td>';
            $row .= '<td><i class="fa fa-clipboard" id="modDuplicateIcon" data-toggle="modal" data-target="#moduleDuplicateModal" style="color:#4c8e0c;"></i></td>';
            $row .= '<td><a href="edit-module/'.$module['id'].'"><i class="fa fa-pencil" style="color:#4c8e0c;"></i></a></td>';
            $row .= '<td><i data-toggle="modal" data-target="#moduleDelModal" class="fa fa-trash" id="dltModOpnIcon" style="color:red;"></i></td>';
            $row .= '</tr>';
                        
        }
        return $row;
    }
    /**
     * Render all module for reorder page
     *
     * @param  array $modules all modules to reorder default empty array
     * @return string          processed table items
     */
    public function renderReorderPageModule($modules=[])
    {
        $row = '';
        foreach ($modules as $module) {
            $moduleOrder = ($module['ordering']) ? $module['ordering'] : '';
            $checked     = ($module['ordering']) ? 'checked' : '';
            $row        .= '<tr id="'.$module['id'].'">';
            $row        .= '<td>'.date('d-M-Y', $module['exam_date']).'</td>';
            $row        .= '<td id="modName">'.$module['moduleName'].'</td>';
            $row        .= '<td>'.'Everyday Study'.'</td>';
            $row        .= '<td>'.$module['subject_name'].'</td>';
            $row        .= '<td><input type="checkbox" id="moduleChecked" '.$checked.'><input type="number" min="1" style="max-width: 54px;margin-left: 65px; border: 1px solid #4995b5;" autocomplete="off" class="moduleOrder" disabled="" value="'.$moduleOrder.'" id="modOrdr">';
            $row        .= '<input type="hidden" id="modId_ordr" name="modId_ordr[]" value="">';
            $row        .= '<input type="hidden" id="modId"  value="'.$module['id'].'">';
            $row        .= '</td>';
            $row        .= '<tr>';
        }

        return $row;

    }//end renderReorderPageModule()


    public function module_preview($modle_id, $question_order_id)
    {
        $data['user_info']       = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['question_info_s'] = $this->tutor_model->getModuleQuestion($modle_id, $question_order_id, null);
        // print_r($data['question_info_s']);die;
        $quesInfo               = json_decode($data['question_info_s'][0]['questionName']);
        $data['total_question'] = $this->tutor_model->getModuleQuestion($modle_id, null, 1);
        $data['page_title']     = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink']     = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']         = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink']     = $this->load->view('dashboard_template/footerlink', $data, true);

        if ($data['question_info_s'][0]['questionType'] == 1) {
            $data['maincontent'] = $this->load->view('module_preview/preview_general', $data, true);
        } else if ($data['question_info_s'][0]['questionType'] == 2) {
            $data['maincontent'] = $this->load->view('module_preview/preview_true_false', $data, true);
        } else if ($data['question_info_s'][0]['questionType'] == 3) {
            $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
            $data['maincontent']             = $this->load->view('module_preview/preview_vocabulary', $data, true);
        } else if ($data['question_info_s'][0]['questionType'] == 4) {
            $data['question_info_vcabulary'] = $quesInfo;
            $data['maincontent']             = $this->load->view('module_preview/preview_multiple_choice', $data, true);
        } else if ($data['question_info_s'][0]['questionType'] == 6) {
            // skip quiz
            $data['numOfRows']    = isset($quesInfo->numOfRows) ? $quesInfo->numOfRows : 0;
            $data['numOfCols']    = isset($quesInfo->numOfCols) ? $quesInfo->numOfCols : 0;
            $data['questionBody'] = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
            $data['questionId']   = $data['question_info_s'][0]['question_id'];
            $quesAnsItem          = $quesInfo->skp_quiz_box;
            $items                = indexQuesAns($quesAnsItem);

            $data['skp_box']     = renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols']);
            $data['maincontent'] = $this->load->view('module/preview/skip_quiz', $data, true);
        } else if ($data['question_info_s'][0]['questionType'] == 8) {
            // assignment
            $data['questionBody']    = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
            $items                   = $quesInfo->assignment_tasks;
            $data['totalItems']      = count($items);
            $data['assignment_list'] = renderAssignmentTasks($items);
            $data['maincontent']     = $this->load->view('module/preview/assignment', $data, true);
        }//end if

        $this->load->view('master_dashboard', $data);

    }//end module_preview()


    public function deleteModule()
    {
        $post  = $this->input->post();
        $clean = $this->security->xss_clean($post);

        $moduleId = $clean['moduleId'];
        $this->ModuleModel->delete($moduleId);

        echo 'true';

    }//end deleteModule()


    /**
     * Render all searched question(check box checked result).
     * This function could be optimized more.
     *
     * @param  array $quesList all question by search params
     * @return string           rendered string
     */
    public function quesSearch()
    {
        $post            = array_filter($this->input->post());
        $post['user_id'] = $this->loggedUserId;
        $quesList        = $this->QuestionModel->search('tbl_question', $post);

        // echo $this->renderSearchedQuestion($quesList);
        $serachedQuesIds = array_column($quesList, 'id');

        $allQuestionType = $this->tutor_model->getAllInfo('tbl_questiontype');
        $questionGroup   = [];
        foreach ($allQuestionType as $questionType) {
            $questionGroup[$questionType['id']] = $this->tutor_model->getUserQuestion('tbl_question', $questionType['id'], $this->loggedUserId);
        }

        $row = '';
        foreach ($allQuestionType as $key) {
            $row .= '<div class="col-md-3"><table class="table table-bordered tbl_ques" id="module_setting2"><thead><tr>';
            $row .= '<th style="">'.$key['questionType'].'<p style="float:right;">Re-Order</p></th></tr></thead><tbody>';
            $i    = 1;
            foreach ($questionGroup[$key['id']] as $question) {
                if (!in_array($question['id'], $serachedQuesIds)) {
                    continue;
                }

                $row .= '<tr><td><div class="form-check"><label class="form-check-label"><label class="form-check-label" for="defaultCheck21">';

                $row .= '<input class="form-check-input1" type="checkbox" value="'.$question['id'].'"  name="moduleQuestion[]" id="quesChecked"> Q'.$i.'<i class="fa fa-info-circle" style="color:orange;"></i> <i class="fa fa-pencil"></i>';

                $row .= '<input type="number" min="1" style="max-width: 54px;margin-left: 65px;" autocomplete="off" class="questionOrder" disabled="disabled" value="" id="qOrdr"><input type="hidden" id="qId_ordr" name="qId_ordr[]" value="">';
                $row .= '<input type="hidden" id="qId"  value="'.$question['id'].'">';

                $row .= '</label></div></td></tr>';
                $i++;
            }

            $row .= '</tbody></table></div>';
        }//end foreach

        echo $row;

    }//end quesSearch()

    public function searchModule()
    {
        $post = $this->input->post();
        $conditions = array_filter($post);
        $conditions['user_id'] = $this->loggedUserId;

        $modules = $this->ModuleModel->search('tbl_module', $conditions);
        $renderedModules = $this->renderAllModule($modules);
        print_r($renderedModules);
    }

}//end class
