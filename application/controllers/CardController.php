<?php 
require_once('stripe-php-master/init.php');
//testing
defined('BASEPATH') OR exit('No direct script access allowed');
class CardController extends CI_Controller {
	 public function __construct(){
      parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		 $this->load->model('SettingModel');
	}
	public function card_form_submit(){				
		//echo '<pre>';print_r($_POST);die();
		if(!empty($_POST['stripeToken'])){
			
			 $token  = $_POST['stripeToken'];


			 $user_email=$this->session->userdata('email');
			 $total_cost=$this->session->userdata('totalCost');
			
			//set api key
			/*$this->db->select('*');
			$this->db->from('tbl_stripe_api_key');
			$this->db->where('type',0);
			$api_key=$this->db->get()->result_array();*/
			  $publish_key=$this->SettingModel->getStripeKey('publish');
		      $sereet_key=$this->SettingModel->getStripeKey('seccreet');
			/*$stripe = array(
			  "secret_key"      => "sk_test_XxfxLa9eNGyO4BMFo3EXcrGl",
			  "publishable_key" => "pk_test_8d5s2El2JNAMmyL1xc87EpcH"
			);*/
			$stripe = array(
			  "secret_key"      => $sereet_key,// $api_key[0]['sereet_key'],
			  "publishable_key" => $publish_key //$api_key[0]['publish_key']
			);

			\Stripe\Stripe::setApiKey($stripe['secret_key']);

			//add customer to stripe
			$customer = \Stripe\Customer::create(array(
				'email' => $user_email,
				'source'  => $token
			));
			$product_variation=rand(100, 999);
			$product = \Stripe\Product::create([
				'name' => 'My SaaS Platform'.'_'. $product_variation,
				'type' => 'service',
			]);
			$plan = \Stripe\Plan::create([
				'currency' => 'usd',
				'interval' => 'month',
				'product' => $product->id,
				'nickname' => 'Pro Plan',
				'amount' => 250,
			]);
			$subscription = \Stripe\Subscription::create([
				'customer' =>  $customer->id,
				'items' => [['plan' =>  $plan->id]],
				
			]);
			
			//item information
			$itemName = "Rs";
			$itemNumber = "1";
			$itemPrice = 250;
			$currency = "usd";
			$orderID = "SKA92712382139";
			
			//charge a credit or a debit card
			$charge = \Stripe\Charge::create(array(
				'customer' => $customer->id,
				'amount'   => $itemPrice,
				'currency' => $currency,
				'description' => $itemName,
				'metadata' => array(
					'order_id' => $orderID
				)
			));
			$invoice=\Stripe\InvoiceItem::create([
				'amount' => 250,
				'currency' => 'usd',
				'customer' => $customer->id,
				'description' => 'One-time setup fee',
			]);
			
			//retrieve charge details
			$chargeJson = $charge->jsonSerialize();
			
			//check whether the charge is successful
			if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
				
				//order details 
				$amount = $chargeJson['amount'];
				$balance_transaction = $chargeJson['balance_transaction'];
				$currency = $chargeJson['currency'];
				$status = $chargeJson['status'];
				$date = date("Y-m-d H:i:s");
				
				//include database config file
				//include_once 'dbConfig.php';
				$data['user_id']=$this->session->userdata('user_id');
				$data['PaymentDate'] = time();
				$paymentType = $this->session->userdata('paymentType');
				if($paymentType == 1){
					$second = 30 * 24 * 3600;
				}elseif($paymentType == 2){
					$second = 30 * 6 * 24 * 3600;
				}elseif($paymentType == 3){
					$second = 30 * 12 * 24 * 3600;
				}
				$data['PaymentEndDate'] = $data['PaymentDate'] + $second;
				$data['total_cost'] = $chargeJson['amount'];
			   // $data['PackageId'] = $_POST['item_number'];
				$data['payment_status'] =  $chargeJson['status'];
				$data['SenderEmail'] =$this->session->userdata('email');
				
				$data['customerId'] =$customer->id;
				$data['subscriptionId'] =$subscription->id;
				$data['paymentType'] =1;
				$data['invoiceId']=$invoice->id;
				$this->db->insert('tbl_payment', $data);
			 	$paymentId=$this->db->insert_id();
				$rs_course=$this->session->userdata('courses');
			foreach($rs_course as $singleCourse)
			{
				$pay['paymentId']=$paymentId;
				$pay['courseId']=$singleCourse;
				$this->db->insert('tbl_payment_details', $pay);
			}
           
            $this->db->set('payment_status', $data['payment_status']);
            $this->db->where('id', $data['user_id']);
            $this->db->update('tbl_useraccount');
			echo '<pre>';print_r($chargeJson);die;
				//insert tansaction data into the database
				$sql = "INSERT INTO orders(name,email,card_num,card_cvc,card_exp_month,card_exp_year,item_name,item_number,item_price,item_price_currency,paid_amount,paid_amount_currency,txn_id,payment_status,created,modified) VALUES('".$name."','".$email."','".$card_num."','".$card_cvc."','".$card_exp_month."','".$card_exp_year."','".$itemName."','".$itemNumber."','".$itemPrice."','".$currency."','".$amount."','".$currency."','".$balance_transaction."','".$status."','".$date."','".$date."')";
				$insert = $db->query($sql);
				$last_insert_id = $db->insert_id;
				
				//if order inserted successfully
				if($last_insert_id && $status == 'succeeded'){
					$statusMsg = "<h2>The transaction was successful.</h2><h4>Order ID: {$last_insert_id}</h4>";
				}else{
					$statusMsg = "Transaction has been failed";
				}
			}else{
				echo 'php error';die();
				$statusMsg = "Transaction has been failed";
			}
		}else{
			echo 'Form error';die();
			$statusMsg = "Form submission error.......";
		}

		//show success or error message
		//echo $statusMsg;
	}
}