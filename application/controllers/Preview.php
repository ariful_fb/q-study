<?php

class Preview extends CI_Controller
{

    public $loggedUserId;
    public function __construct() {
        parent::__construct();
        
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        $this->loggedUserId = $user_id;

        if($user_id == NULL && $user_type == NULL){
            redirect('welcome');
        }
        if($user_type != 3){
            redirect('welcome');
        }
        $this->load->model('Preview_model');
        $this->load->library('form_validation');
        $this->load->library('upload');
    }
    
    public function question_preview($question_id) 
    {
        $data['user_info']=$this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['question_info']=$this->Preview_model->getInfo('tbl_question', 'id', $question_id);
        $data['question_id'] = $question_id;
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);        
        $data['maincontent'] = $this->load->view('preview/question_image', $data, TRUE);
        $this->load->view('master_dashboard', $data);   
    }
    public function answer_matching(){
      $question_id = $_POST['id'];

      $text = $_POST['user_answer'];
      $find = array('&nbsp;','\n','\t','\r');
      $repleace = array('','','','');		
      $text = strip_tags($text);
      $text = str_replace($find,$repleace,$text);
      $text = trim($text);

      $answer_info=$this->Preview_model->getInfo('tbl_question','id',$question_id);

      $text_1 = $answer_info[0]['answer'];
      $find = array('&nbsp;','\n','\t','\r');
      $repleace = array('','','','');		
      $text_1 = strip_tags($text_1);
      $text_1 = str_replace($find,$repleace,$text_1);
      $text_1 = trim($text_1);
      if($text == $text_1){
       echo 1;
   }else{
       echo 0;
   }
}

	//    Preview Vocubulary Part
public function preview_vocubulary($question_id) {
    $data['user_info'] = $this->Preview_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
    $question_info = $this->Preview_model->getInfo('tbl_question', 'id', $question_id);
//        $question_info = $this->Preview_model->questionInfo($question_id);
    $data['question_info'] = json_decode($question_info[0]['questionName']);
//        echo '<pre>';print_r($data['question_info']->definition);die;

    $data['question_id'] = $question_id;

    $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
    $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
    $data['header'] = $this->load->view('dashboard_template/header', $data, true);
    $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
    $data['maincontent'] = $this->load->view('preview/preview_vocubulary', $data, TRUE);
    $this->load->view('master_dashboard', $data);
}
}
