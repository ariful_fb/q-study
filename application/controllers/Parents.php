<?php


class Parents extends CI_Controller{
    public function __construct() {
        parent::__construct();
        
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        if($user_id == NULL && $user_type == NULL){
            redirect('welcome');
        }
         $this->load->model('Parent_model');
		 $this->load->helper(array('form', 'url'));
	     $this->load->library('form_validation');
		 $this->load->library('upload');
    }
    
     public function index() 
	 {
		$data['user_info']=$this->Parent_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
		$data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);        
        $data['maincontent'] = $this->load->view('parents/parent_dashboard', $data, TRUE);
        $this->load->view('master_dashboard', $data);
    }
	public function parent_setting()
	{
		
		$data['user_info']=$this->Parent_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
		$data['page_title'] = '.:: Q-Study :: Tutor yourself...';
		$data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);        
        $data['maincontent'] = $this->load->view('parents/parent_setting', $data, TRUE);
        $this->load->view('master_dashboard', $data);
	}
	public function my_details()
	{
		$data['user_info']=$this->Parent_model->userInfo($this->session->userdata('user_id'));
		
		$data['page_title'] = '.:: Q-Study :: Tutor yourself...';
		$data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);        
        $data['maincontent'] = $this->load->view('parents/my_details', $data, TRUE);
        $this->load->view('master_dashboard', $data);
	}
	public function update_my_details()
	{
		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[6]|min_length[5]');
		$this->form_validation->set_rules('passconf', 'passconf', 'trim|required|matches[password]');
		if($this->form_validation->run()==false)
		{
			echo 0;
		}else{
			$password=md5($this->input->post('password'));
			$data = array(
					'user_pawd' =>$password
			);
			$this->Parent_model->updateInfo('tbl_useraccount','id',$this->session->userdata('user_id'),$data);
			echo 1;
		}
	}
	public function upload_photo()
	{
		$data['user_info']=$this->Parent_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
		$data['page_title'] = '.:: Q-Study :: Tutor yourself...';
		$data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);        
        $data['maincontent'] = $this->load->view('parents/upload', $data, TRUE);
        $this->load->view('master_dashboard', $data);
	}
	private function upload_user_photo_options()
	{
		$config = array();
		$config['upload_path'] = './assets/uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		// $config['max_width'] = 1080;
		// $config['max_height'] = 640;
		// $config['min_width']  = 150;
		// $config['min_height'] = 150;
		$config['overwrite']  = FALSE;
		return $config;
	}
	public function parent_dropzone_file()
	{
		$this->upload->initialize($this->upload_user_photo_options());
		 if ( ! $this->upload->do_upload('file')){
			 echo 0;
		 }else{
			$imageName=$this->upload->data();
			$user_profile_picture=$imageName['file_name'];
			$data = array(
				'image' =>$user_profile_picture
			);
			$rs['res']=$this->Parent_model->updateInfo('tbl_useraccount','id',$this->session->userdata('user_id'),$data);
			echo 1;
		 }
		
	}
}
