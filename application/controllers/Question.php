<?php
/**
 * Question controller class.
 * 
 * @author Shakil Ahmed (initial part author)
 */

class Question extends CI_Controller
{

    public $loggedUserId, $loggedUserType;


    public function __construct()
    {
        parent::__construct();

        $user_id              = $this->session->userdata('user_id');
        $user_type            = $this->session->userdata('userType');
        $this->loggedUserId   = $user_id;
        $this->loggedUserType = $user_type;

        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }

        $this->load->model('QuestionModel');

    }//end __construct()


    /**
     * Delete a question from database.
     *
     * @param integer $questionId question to delete
     *
     * @return void
     */
    public function deleteQuestion($questionId=0)
    {
        
        /*delete question info from tbl_question
        delete all question module relationship*/
        $delItems = $this->QuestionModel->delete('tbl_question', 'id', $questionId);
        $this->QuestionModel->delete('tbl_modulequestion', 'question_id', $questionId);
        if ($delItems) {
            echo 'true';
        } else {
            echo 'false';
        }

    }//end deleteQuestion()


    /**
     * This method will simply duplicate a question
     *
     * @param integer $questionId question to duplicate
     *
     * @return void
     */
    public function duplicateQuestion($questionId=0)
    {
        $parentQuestion = $this->QuestionModel->info($questionId);
        unset($parentQuestion['id']);
        if (count($parentQuestion)) {
            $this->QuestionModel->insert('tbl_question', $parentQuestion);
            echo 'true';
        } else {
            echo 'false';
        }

    }//end duplicateQuestion()


}//end class
