<?php
/**
 * Tutor Class
 */
class Tutor extends CI_Controller
{

    public $loggedUserId;


    public function __construct()
    {
        parent::__construct();

        $user_id            = $this->session->userdata('user_id');
        $user_type          = $this->session->userdata('userType');
        $this->loggedUserId = $user_id;

        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }

        if ($user_type != 3) {
            redirect('welcome');
        }

        $this->load->model('Parent_model');
        $this->load->model('tutor_model');
        $this->load->model('Student_model');
        $this->load->model('ModuleModel');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->model('Preview_model');
        $this->load->model('QuestionModel');

    }//end __construct()


    public function index()
    {
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']     = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('tutors/tutors_dashboard', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end index()


    public function tutor_setting()
    {
        $data['user_info']   = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title']  = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink']  = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']      = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink']  = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('tutors/tutor_setting', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end tutor_setting()


    public function tutor_details()
    {
        $data['user_info']   = $this->tutor_model->userInfo($this->session->userdata('user_id'));
        $data['page_title']  = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink']  = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']      = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink']  = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('tutors/tutor_details', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end tutor_details()


    public function update_tutor_details()
    {
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[6]|min_length[5]');
        $this->form_validation->set_rules('passconf', 'passconf', 'trim|required|matches[password]');
        if ($this->form_validation->run() == false) {
            echo 0;
        } else {
            $password = md5($this->input->post('password'));
            $data     = ['user_pawd' => $password];
            $this->tutor_model->updateInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'), $data);
            echo 1;
        }

    }//end update_tutor_details()


    public function tutor_upload_photo()
    {
        $data['user_info']   = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['page_title']  = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink']  = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header']      = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink']  = $this->load->view('dashboard_template/footerlink', $data, true);
        $data['maincontent'] = $this->load->view('school/upload', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end tutor_upload_photo()


    private function upload_user_photo_options()
    {
        $config                  = [];
        $config['upload_path']   = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';

        $config['overwrite'] = false;
        return $config;

    }//end upload_user_photo_options()


    public function tutor_file_upload()
    {
        $this->upload->initialize($this->upload_user_photo_options());
        if (! $this->upload->do_upload('file')) {
            echo 0;
        } else {
            $imageName            = $this->upload->data();
            $user_profile_picture = $imageName['file_name'];
            $data                 = ['image' => $user_profile_picture];
            $rs['res']            = $this->tutor_model->updateInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'), $data);
            echo 1;
        }

    }//end tutor_file_upload()


    public function view_course()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $data['maincontent'] = $this->load->view('tutors/view_course', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end view_course()


    public function all_module()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $user_id = $this->session->userdata('id');

        $data['user_info']       = $this->tutor_model->userInfo($user_id);
        $data['all_module']      = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_grade']       = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type'] = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course']      = $this->tutor_model->getAllInfo('tbl_course');

        $data['maincontent'] = $this->load->view('tutors/module/all_module', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end all_module()


    public function add_module()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $user_id = $this->session->userdata('id');

        $data['user_info']       = $this->tutor_model->userInfo($user_id);
        $data['all_module']      = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_grade']       = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type'] = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course']      = $this->tutor_model->getAllInfo('tbl_course');

        $data['maincontent'] = $this->load->view('tutors/module/add_module', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end add_module()


    public function question_list() {
        $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $user_id = $this->session->userdata('user_id');

        $data['user_info'] = $this->tutor_model->userInfo($user_id);
        $data['all_module'] = $this->tutor_model->getInfo('tbl_module', 'user_id', $user_id);
        $data['all_grade'] = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_module_type'] = $this->tutor_model->getAllInfo('tbl_moduletype');
        $data['all_course'] = $this->tutor_model->getAllInfo('tbl_course');
        
        $data['all_question_type'] = $this->tutor_model->getAllInfo('tbl_questiontype');
        foreach ($data['all_question_type'] as $row){
            $question_list[$row['id']] = $this->tutor_model->getUserQuestion('tbl_question',$row['id'],$user_id);
        }
        
        $data['all_question'] = $question_list;
        $data['maincontent'] = $this->load->view('tutors/question/question_list', $data, TRUE);
        $this->load->view('master_dashboard', $data);
    }


    public function general_question()
    {
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        $user_id             = $this->session->userdata('user_id');
        $data['all_grade']   = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->tutor_model->getInfo('tbl_subject', 'created_by', $user_id);

        $data['maincontent'] = $this->load->view('tutors/question/general_question', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end general_question()


    public function create_question($item)
    {
        $data['headerlink']  = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']      = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink']  = $this->load->view('dashboard_template/footerlink', '', true);
        $user_id             = $this->session->userdata('user_id');
        $data['all_grade']   = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->tutor_model->getInfo('tbl_subject', 'created_by', $user_id);
        $question_box        = 'tutors/question/question-box';
        if ($item == 1) {
            $question_box .= '/general';
        } else if ($item == 2) {
            $question_box .= '/true-false';
        } else if ($item == 3) {
            $question_box .= '/vocabulary';
        } else if ($item == 6) {
            $question_box .= '/skip_quiz';
        } else if ($item == 8) {
            $question_box .= '/assignment';
        }

        $data['question_box'] = $this->load->view($question_box, '', true);
        $data['maincontent']  = $this->load->view('tutors/question/create_question', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end create_question()


    public function add_subject_name()
    {
        $data['created_by']   = $this->session->userdata('user_id');
        $data['subject_name'] = $this->input->post('subject_name');

        $this->tutor_model->insertInfo('tbl_subject', $data);

        $all_tutor_subject = $this->tutor_model->getInfo('tbl_subject', 'created_by', $data['created_by']);
        echo '<option value="">Select ...</option>';
        foreach ($all_tutor_subject as $row) {
            echo '<option value="'.$row['subject_id'].'" onchange="getChapter('.$row['subject_id'].')">'.$row['subject_name'].'</option>';
        }

    }//end add_subject_name()


    public function get_chapter_name()
    {
        $subject_id = $this->input->post('subject_id');

        $all_subject_chapter = $this->tutor_model->getInfo('tbl_chapter', 'subjectId', $subject_id);
        echo '<option value="">Select Chapter</option>';
        foreach ($all_subject_chapter as $chapter) {
            echo '<option value="'.$chapter['id'].'">'.$chapter['chapterName'].'</option>';
        }

    }//end get_chapter_name()


    public function save_question_data()
    {
        $post           = $this->input->post();
        $clean          = $this->security->xss_clean($post);
        $clean['media'] = isset($_FILES) ? $_FILES : [];
        $questionName   = '';
        $answer         = '';

        $data['questionType'] = $this->input->post('questionType');

        if ($data['questionType'] == 3) {
            // vocabulary
            $questionName = $this->processVocabulary($clean);
        } else if ($data['questionType'] == 6) {
            // skip quiz
            $temp['question_body'] = isset($clean['question_body']) ? $clean['question_body'] : '';
            $temp['skp_quiz_box']  = $clean['ques_ans'];
            $temp['numOfRows']     = isset($clean['numOfRows']) ? $clean['numOfRows'] : 0;
            $temp['numOfCols']     = isset($clean['numOfCols']) ? $clean['numOfCols'] : 0;
            $questionName          = json_encode($temp);
            $answer                = json_encode(array_values(array_filter($clean['ans'])));
        } else if ($data['questionType'] == 8) {
            // assignment
            $temp         = $this->processAssignmentTasks($clean);
            $questionName = json_encode($temp);
        }

            $data['studentgrade'] = $this->input->post('studentgrade');
            $data['subject']      = $this->input->post('subject');
            $data['chapter']      = '';
        // $this->input->post('chapter');
            $data['questionName'] = $questionName;
        // $this->input->post('questionName');
            $data['answer'] = $answer;
        // $this->input->post('answer');
            $data['questionDescription'] = $this->input->post('questionDescription');
            $data['isCalculator']        = $this->input->post('isCalculator');

            $hour   = $this->input->post('hour');
            $minute = $this->input->post('minute');
            $second = $this->input->post('second');

            $data['questionTime'] = $hour.':'.$minute.':'.$second;

            $questionId = $this->tutor_model->insertInfo('tbl_question', $data);

        if (isset($_FILES)) {
            // if file exists upload, tested only for vocabulary
            $this->questionMediaUpload($questionId);
        }

            redirect('Tutor/preview/'.$questionId);

    }//end save_question_data()


    /**
     * grab item from post method for question field
     *
     * @param  array $items post array
     * @return string        json encoded data to record as question
     */
    public function processVocabulary($items)
    {
        $arr['word']            = $items['word'];
        $arr['definition']      = $items['definition'];
        $arr['parts_of_speech'] = $items['parts_of_speech'];
        $arr['synonym']         = $items['synonym'];
        $arr['antonym']         = $items['antonym'];
        $arr['sentence']        = $items['sentence'];
        $arr['near_antonym']    = $items['near_antonym'];
        return json_encode($arr);

    }//end processVocabulary()


    /**
     * question media file upload and record
     *
     * @param  integer $questionId questionId for files to link with
     * @return void
     */
    public function questionMediaUpload($questionId=0)
    {
        $files                 = $_FILES;
        $dataToInsert          = [];
        $config['upload_path'] = 'assets/uploads/question_media';

        $config['allowed_types'] = 'mp3|mp4|3gp|ogg';
        $config['max_size']      = 18403791;
        $config['max_width']     = 1024;
        $config['max_height']    = 768;

        foreach ($files as $index => $item) {
            $config['file_name'] = uniqid();
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload($index);

            $fileName = $this->upload->data('file_name');
            $filePath = $this->upload->data('file_path');
            $fileType = $this->upload->data('file_type');

            $dataToInsert['media_url']   = $filePath.$fileName;
            $dataToInsert['media_type']  = $fileType;
            $dataToInsert['upload_date'] = date('Y-m-d H:i:s');
            $dataToInsert['question_id'] = $questionId;
            $this->tutor_model->insertInfo('tbl_question_media', $dataToInsert);
        }

    }//end questionMediaUpload()


    /**
     * preview question type skip quiz
     *
     * @param  integer $questionId questionId
     * @return void
     */
    public function preview($questionId)
    {
        $quesInfo     = $this->Preview_model->getInfo('tbl_question', 'id', $questionId);
        $questionType = $quesInfo[0]['questionType'];
        $quesInfo     = json_decode($quesInfo[0]['questionName']);

        // common view file
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);

        if ($questionType == 8) {
            // Assignment
            $questionBody            = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
            $data['questionBody']    = $questionBody;
            $items                   = $quesInfo->assignment_tasks;
            $data['totalItems']      = count($items);
            $data['assignment_list'] = $this->renderAssignmentTasks($items);
            $data['maincontent']     = $this->load->view('tutors/question/preview/assignment', $data, true);
        } else if ($questionType == 6) {
            // skip quiz
            $data['numOfRows']    = isset($quesInfo->numOfRows) ? $quesInfo->numOfRows : 0;
            $data['numOfCols']    = isset($quesInfo->numOfCols) ? $quesInfo->numOfCols : 0;
            $data['questionBody'] = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
            $data['questionId']   = $questionId;
            $quesAnsItem          = $quesInfo->skp_quiz_box;
            $items                = $this->indexQuesAns($quesAnsItem);

            $data['skp_box'] = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols']);

            $user_id             = $this->session->userdata('user_id');
            $data['all_grade']   = $this->tutor_model->getAllInfo('tbl_studentgrade');
            $data['all_subject'] = $this->tutor_model->getInfo('tbl_subject', 'created_by', $user_id);
            $data['maincontent'] = $this->load->view('tutors/question/preview/skp_quiz', $data, true);
        }//end if

        $this->load->view('master_dashboard', $data);

    }//end preview()


    /**
     * Before passing items to renderSkpQuizPrevTable() index it first with this func
     * basically for preview skip quiz table
     *
     * @param array $items Json object array.
     *
     * @return array        Array with proper indexing
     */
    public function indexQuesAns($items)
    {
        $arr = [];
        foreach ($items as $item) {
            $temp            = json_decode($item);
            $cr              = explode('_', $temp->cr);
            $col             = $cr[0];
            $row             = $cr[1];
            $arr[$col][$row] = [
                'type' => $temp->type,
                'val'  => $temp->val,
            ];
        }

        return $arr;

    }//end indexQuesAns()


    /**
     * Render the indexed item to table data for preview
     *
     * @param  array   $items   ques ans as indexed item(get processed items from indexQuesAns())
     * @param  integer $rows    num of row in table
     * @param  integer $cols    num of cols in table
     * @param  integer $showAns optional, set 1 will show the answers too
     * @return string           table item
     */
    public function renderSkpQuizPrevTable($items, $rows, $cols, $showAns=0, $pageType='')
    {
        // print_r($items);die;
        $row = '';
        for ($i = 1; $i <= $rows; $i++) {
            $row .= '<tr>';
            for ($j = 1; $j <= $cols; $j++) {
                if ($items[$i][$j]['type'] == 'q') {
                    $row .= '<td><input type="button" data_q_type="0" data_num_colofrow="" value="'.$items[$i][$j]['val'].'" name="skip_counting[]" class="form-control rsskpin input-box  rsskpinpt'.$i.'_'.$j.'" readonly style="min-width:50px; max-width:50px; background-color:#ffb7c5;">';
                    if ($pageType = 'edit') {
                        $quesObj = [
                            'cr'   => $j.'_'.$i,
                            'val'  => $items[$i][$j]['val'],
                            'type' => 'q',
                        ];
                        $quesObj = json_encode($quesObj);
                        $row    .= '<input type="hidden" value=\''.$quesObj.'\' name="ques_ans[]" id="obj">';
                        // $row .= '<input type="hidden" value=\''.$quesObj.'\' name="ans[]" id="ans_obj">';
                    }

                    $row .= '</td>';
                } else {
                    $ansObj = [
                        'cr'   => $i.'_'.$j,
                        'val'  => $items[$i][$j]['val'],
                        'type' => 'a',
                    ];
                    $ansObj = json_encode($ansObj);
                    $val    = ($showAns == 1) ? ' value="'.$items[$i][$j]['val'].'"' : '';

                    $row .= '<td><input autocomplete="off" type="text" '.$val.' data_q_type="0" data_num_colofrow="'.$i.'_'.$j.'" value="" name="skip_counting[]" class="form-control rsskpin input-box ans_input  rsskpinpt'.$i.'_'.$j.'"  style="min-width:50px; max-width:50px;background-color:#baffba;">';
                    $row .= '<input type="hidden" value="" name="given_ans[]" id="given_ans">';
                    if ($pageType = 'edit') {
                        $row .= '<input type="hidden" value="" name="ques_ans[]" id="obj">';
                        $row .= '<input type="hidden" value=\''.$ansObj.'\' name="ans[]" id="ans_obj">';
                    }

                    $row .= '</td>';
                }//end if
            }//end for

            $row .= '</tr>';
        }//end for

        return $row;

    }//end renderSkpQuizPrevTable()


    /**
     * Check given skip box answers with the stored answers
     * can give wrong value given indices
     *
     * @return void
     */
    public function checkSkpboxAnswer()
    {
        $post       = $this->input->post();
        $questionId = $this->input->post('questionId');
        $givenAns   = $this->indexQuesAns($post['given_ans']);

        $temp     = $this->Preview_model->getInfo('tbl_question', 'id', $questionId);
        $savedAns = $this->indexQuesAns(json_decode($temp[0]['answer']));

        $temp2     = json_decode($temp[0]['questionName']);
        $numOfRows = $temp2->numOfRows;
        $numOfCols = $temp2->numOfCols;
        // echo $numOfRows .' ' . $numOfCols;
        $wrongAnsIndices = [];

        for ($row = 1; $row <= $numOfRows; $row++) {
            for ($col = 1; $col <= $numOfCols; $col++) {
                if (isset($savedAns[$row][$col])) {
                    $wrongAnsIndices[] = ($savedAns[$row][$col] != $givenAns[$row][$col]) ? $row.'_'.$col : null;
                }
            }
        }

        $wrongAnsIndices = array_filter($wrongAnsIndices);
        if (count($wrongAnsIndices)) {
            $this->session->set_flashdata('wrong_ans', 'wrong_ans');
        } else {
            $this->session->set_flashdata('right_ans', 'Congres righ answer');
        }

        redirect('Tutor/Preview/'.$questionId);

    }//end checkSkpboxAnswer()


    /**
     * get right answers for a question, get hit from ajax call
     *
     * @return string table item
     */
    public function getRightAns()
    {
        $post       = $this->input->post();
        $questionId = $post['qId'];
        $temp       = $this->Preview_model->getInfo('tbl_question', 'id', $questionId);
        $quesAns    = json_decode($temp[0]['questionName']);
        $items      = $this->indexQuesAns($quesAns->skp_quiz_box);
        // print_r($items);
        $rows    = $quesAns->numOfRows;
        $cols    = $quesAns->numOfCols;
        $tblData = $this->renderSkpQuizPrevTable($items, $rows, $cols, $showAns = 1);
        echo $tblData;

    }//end getRightAns()


    /**
     * Process assignment type question form input
     * format and index accordingly to save as question body
     *
     * @param array $items Form input.
     *
     * @return string        Json object string
     */
    public function processAssignmentTasks(array $items)
    {
        $itemNum = count($items['qMark']);
        $arr     = [];
        $temp    = [];
        for ($a = 0; $a < $itemNum; $a++) {
            $arr[] = json_encode(
                [
                    'serial'      => $a,
                    'qMark'       => $items['qMark'][$a],
                    'obtnMark'    => $items['obtnMark'][$a],
                    'description' => $items['descriptions'][$a],
                ]
            );
        }

        $temp['question_body']    = $items['question_body'];
        $temp['assignment_tasks'] = $arr;
        return $temp;

    }//end processAssignmentTasks()


    /**
     * Make table row element with assignment tasks
     *
     * @param  array $items assignment tasks json array
     * @return string        table row element
     */
    public function renderAssignmentTasks(array $items, $pageType='')
    {
        $row = '';
        foreach ($items as $task) {
            $task     = json_decode($task);
            $qMark    = $task->qMark;
            $obtnMark = $task->obtnMark;
            if ($pageType == 'edit') {
                $qMark    = '<input name="qMark[]" class="form-control" type="text" value="'.$qMark.'"'.' type="number" step="0.1" required>';
                $obtnMark = '<input name="obtnMark[]" class="form-control" type="text" value="'.$obtnMark.'"'.'type="number" step="0.1" required>';
            }

            $row .= '<tr id="'.($task->serial + 1).'">';
            $row .= '<td>'.($task->serial + 1).'</td>';
            $row .= '<td>'.$qMark.'</td>';
            $row .= '<td>'.$obtnMark.'</td>';
            $row .= '<td><i class="fa fa-eye qDtlsOpenModIcon" data-toggle="modal" data-target="#quesDtlsModal"></i></td>';
            $row .= '<input name="descriptions[]" type="hidden" id="hiddenTaskDesc" value="'.$task->description.'">';
            $row .= '</tr>';
        }

        return $row;

    }//end renderAssignmentTasks()


    /**
     * Edit question(view part)
     * Will save submitted data from updateQuestionInfo()
     *
     * @param integer $questionType Question type.
     * @param integer $questionId   Question id to edit.
     *
     * @return void
     */
    public function editQuestion($questionType, $questionId)
    {
        $user_id             = $this->session->userdata('user_id');
        $data['all_grade']   = $this->tutor_model->getAllInfo('tbl_studentgrade');
        $data['all_subject'] = $this->tutor_model->getInfo('tbl_subject', 'created_by', $user_id);

        // Question Info.
        $quesInfo = $this->Preview_model->getInfo('tbl_question', 'id', $questionId);
        $quesInfo = json_decode($quesInfo[0]['questionName']);

        // Common view file.
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', '', true);
        $data['header']     = $this->load->view('dashboard_template/header', '', true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', '', true);
        if ($questionType == 1) {
            // general
        } else if ((int) $questionType === 2) {
            // True-false.
        } else if ((int) $questionType === 3) {
            // Vocabulary.
        } else if ((int) $questionType === 4) {
            // Multiple choice.
        } else if ((int) $questionType === 5) {
            // Multiple response.
        } else if ((int) $questionType === 6) {
            // Skip quiz.
            $items                 = $this->indexQuesAns($quesInfo->skp_quiz_box);
            $data['numOfRows']     = $quesInfo->numOfRows;
            $data['numOfCols']     = $quesInfo->numOfCols;
            $data['skp_box']       = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols'], $showAns = 1, 'edit');
            $data['questionId']    = $questionId;
            $data['questionBody']  = $quesInfo->question_body;
            $data['edit_question'] = $this->load->view('tutors/question/edit/skip_quiz', $data, true);
        } else if ((int) $questionType === 7) {
            // Matching.
            echo 'hit2';
        } else if ((int) $questionType === 8) {
            // Assignment type.
            $items                   = $quesInfo->assignment_tasks;
            $data['assignment_list'] = $this->renderAssignmentTasks($items, 'edit');
            $data['questionBody']    = $quesInfo->question_body;
            $data['qId']             = $questionId;
            $data['edit_question']   = $this->load->view('tutors/question/edit/assignment', $data, true);
        }//end if

        $data['maincontent'] = $this->load->view('tutors/question/question-box/edit_question_common_part', $data, true);
        $this->load->view('master_dashboard', $data);

    }//end editQuestion()


    /**
     * Save requested question info to database
     *
     * @return void
     */
    public function updateQuestionInfo()
    {
        $post         = $this->input->post();
        $clean        = $this->security->xss_clean($post);
        $questionName = '';
        $questionType = $post['questionType'];
        $answer       = '';
        $hour         = isset($post['hour']) ? $post['hour'] : 0;
        $min          = isset($post['minute']) ? $post['minute'] : 0;
        $sec          = isset($post['second']) ? $post['second'] : 0;

        $questionId = $post['questionId'];
        if ($questionType == 1) {
            // general
        } else if ($questionType == 2) {
            // true-false
        } else if ($questionType == 3) {
            // vocabulary
        } else if ($questionType == 4) {
            // multiple choice
        } else if ($questionType == 5) {
            // multiple response
        } else if ($questionType == 6) {
            // skip quiz
            $temp['question_body'] = isset($clean['question_body']) ? $clean['question_body'] : '';
            $temp['skp_quiz_box']  = array_unique(array_filter(array_merge($clean['ques_ans'], $clean['ans'])));
            // may be a big mistake, or it may find a chipa goli to get fixed;
            // print_r($temp['skp_quiz_box']);die;
            $temp['numOfRows'] = isset($clean['numOfRows']) ? $clean['numOfRows'] : 0;
            $temp['numOfCols'] = isset($clean['numOfCols']) ? $clean['numOfCols'] : 0;

            $questionName = json_encode($temp);
            $answer       = json_encode(array_values(array_filter($clean['ans'])));
        } else if ($questionType == 7) {
            // matching
        } else if ($questionType == 8) {
            // Assignment
            $questionName = json_encode($this->processAssignmentTasks($post));
        }//end if

        $data = [
            'questionType' => $post['questionType'],
            'chapter'      => isset($post['subject']) ? $post['subject'] : '',
            'subject'      => $post['subject'],
            'studentgrade' => $post['studentgrade'],
            'questionName' => $questionName,
            'answer'       => $answer,
            'questionTime' => $hour.':'.$min.':'.$sec,
            'isCalculator' => isset($post['isCalculator']) ? $post['isCalculator'] : null,
            'updated_at'   => time(),
        ];

        $this->QuestionModel->update('tbl_question', 'id', $questionId, $data);
        $this->session->set_flashdata('success_msg', 'Question Updated Successfully');
        redirect("Tutor/preview/$questionId");

    }//end updateQuestionInfo()

    public function test()
    {
       $this->load->view('test');
    }


}//end class
