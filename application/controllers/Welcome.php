<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();

       $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');        
        if ($user_id != NULL && $user_type != NULL) {
            redirect('dashboard');
        }
    }

    public function index() {
        $data['header'] = $this->load->view('common/header', '', true);
        $data['menu'] = $this->load->view('menu', '', true);
        $data['footer_link'] = $this->load->view('common/footer_link', '', true);
        $data['footer'] = $this->load->view('common/footer', '', true);
        $this->load->view('index', $data);
    }

}
