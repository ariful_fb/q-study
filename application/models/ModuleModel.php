<?php

class ModuleModel extends CI_Model
{


    public function moduleName($moduleId)
    {
        $res = $this->db
        ->select('moduleName')
        ->where('id', $moduleId)
        ->get('tbl_module')
        ->result_array();

        return isset($res[0]['moduleName']) ? $res[0]['moduleName'] : '';

    }//end moduleName()


    /**
     * Return module type name by a module Id
     *
     * @param integer $moduleTypeId module type id
     *
     * @return string  module type name
     */
    public function moduleTypeName($moduleTypeId)
    {
        $res = $this->db
        ->select('module_type')
        ->where('id', $moduleTypeId)
        ->get('tbl_moduletype')
        ->result_array();

        return isset($res[0]['module_type']) ? $res[0]['module_type'] : '';

    }//end moduleTypeName()


    /**
     * return all module type
     *
     * @return array          all module type
     */
    public function allModuleType()
    {
        return $this->db
        ->select('*')
        ->get('tbl_moduletype')
        ->result_array();
        
    }//end allModuleType()


    /**
     * Insert batch operation will record multiple data at a time.
     * 
     * @param  string $tableName table name
     * @param  array $data      $arr = [ 0=>[1,2,3], 1=[1,2,3] ]
     * 
     * @return mixed            insert id or null if failed to insert
     */
    public function insert($tableName, $data)
    {
        $res = $this->db
        ->insert_batch($tableName, $data);

        return $res ? $this->db->insert_id() : NULL;
    }


    /**
     * This function will delete module info 
     * and attached question from module question pivot table.
     * 
     * @param  integer $moduleId module id to perform delete
     * @return bool           delete success/failed
     */
    public function delete($moduleId)
    {
        $this->db
        ->where('id', $moduleId)
        ->delete('tbl_module');

        $this->db
        ->where('module_id', $moduleId)
        ->delete('tbl_modulequestion');

        return 1;
    }

    /**
     * Delete module question from module question table.
     * @param  integer $moduleId module id
     * @return void
     */
    public function deleteModuleQuestion($moduleId)
    {
        $this->db
        ->where('module_id', $moduleId)
        ->delete('tbl_modulequestion');
    }

    /**
     * Update multiple column with desired value
     *
     * @param  string $tableName    table to get affect
     * @param  array  $dataToUpdate array of data to update 
     * ex:[  0=>['column_name'=>value],
     *       1=['abc'=>'def']
     *    ]
     * @param  string $selector     selector column on which where condition will apply
     * 
     * @return null
     */
    public function update($tableName, $dataToUpdate, $selector)
    {
        //$dataToUpdate['updated_at'] = date("Y-m-d H:i:s");

        $this->db
        ->update_batch($tableName, $dataToUpdate, $selector);

    }//end update()


    /**
     * Get module Info
     * 
     * @param  integer $moduleId module id
     * 
     * @return array           module info
     */
    public function moduleInfo($moduleId)
    {
        $res = $this->db
        ->where('id', $moduleId)
        ->get('tbl_module')
        ->result_array();

        return isset($res[0])?$res[0]:[];
    }//end moduleInfo()

    /**
     * Get all question ids of a module
     * @param  integer $moduleId module id
     * @return array   module question ids and orders
     * 
     */
    public function moduleQuestion($moduleId)
    {
        $res = $this->db
        ->where('module_id', $moduleId)
        ->get('tbl_modulequestion')
        ->result_array();

        return $res;
    }

    /**
     * Get all module for a loggedUser(without parent and student user).
     * @return array all module of a user
     */
    public function allModule($conditions = [])
    {
        $loggedUserId = $this->session->userdata('user_id');
        $this->db
             ->select('tbl_module.*, tbl_subject.subject_name as subject_name, tbl_chapter.chapterName as chapterName')
             ->join('tbl_subject', 'tbl_subject.subject_id=tbl_module.subject', 'left')
             ->join('tbl_chapter', 'tbl_chapter.id=tbl_module.chapter', 'left');
              
              if(count($conditions)){
                    $this->db->where($conditions);
              } else {
                    $this->db->where('user_id', $loggedUserId);
              }
              
        $res = $this->db->get('tbl_module') ->result_array();
        
        return $res;
    }

    /**
     * search from any table using conditions
     * @param  string $tableName table to perform search
     * @param  array  $params    conditions array
     * @return [type]            [description]
     */
    public function search($tableName,$params)
    {
        $res = $this->db
        ->where($params)
        ->get($tableName)
        ->result_array();

        return $res;
    }


}//end class
