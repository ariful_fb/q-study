<?php

class Tutor_model extends CI_Model
{


    public function insertInfo($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();

    }//end insertInfo()


    public function insertId($table, $data)
    {
        $this->db->insert($table, $data);

        $insert_id = $this->db->insert_id();
        return $insert_id;

    }//end insertId()


    public function getAllInfo($table)
    {
        $this->db->select('*');
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();

    }//end getAllInfo()


    public function get_all_where($select, $table, $columnName, $columnValue)
    {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($columnName, $columnValue);

        $query = $this->db->get();
        return $query->result_array();

    }//end get_all_where()


    public function getSelectItem($select, $table)
    {
        $this->db->select($select);
        $this->db->from($table);

        $query = $this->db->get();
        return $query->result_array();

    }//end getSelectItem()


    public function updateInfo($table, $colName, $colValue, $data)
    {
        $this->db->where($colName, $colValue);
        $this->db->update($table, $data);

    }//end updateInfo()


    public function deleteInfo($table, $colName, $colValue)
    {
        $this->db->where($colName, $colValue);
        $this->db->delete($table);

    }//end deleteInfo()


    public function getInfo($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->result_array();

    }//end getInfo()


    public function getRow($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->row_array();

    }//end getRow()


    // Module Section
    public function userInfo($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_useraccount');

        $this->db->join('tbl_country', 'tbl_useraccount.country_id = tbl_country.id', 'LEFT');
        $this->db->where('tbl_useraccount.id', $user_id);

        $query = $this->db->get();
        return $query->row_array();

    }//end userInfo()


    /**
     * Get all students of a specific tutor/school/parent
     *
     * @param array $conditions [column_name=>value,...],
     * for tutor,qstudy etc:['sct_id'=>loggedUserId]
     *
     * @return array             studentIds ex:[1,2,3,4,5]
     */
    public function allStudents(array $conditions=[])
    {
        $loggedUserId   = $this->session->userdata('user_id');
        $loggedUserType = $this->session->userdata('userType');

        if ((int) $loggedUserType === 1) {
            // Parent.
            $res = $this->db
            ->select('id as `st_id`')
            ->where('parent_id', $loggedUserId)
            ->get('tbl_useraccount')
            ->result_array();
        } else {
            // Corporate/school/tutor/qstudy.
            $res = $this->db
            ->select('st_id')
            ->where($conditions)
            ->get('tbl_enrollment')
            ->result_array();
        }

        return array_column($res, 'st_id');

    }//end allStudents()


    /**
     * Return all module type
     *
     * @param  integer $tutorId tutor id
     * @return array          all module type
     */
    public function allModuleType()
    {
        $this->db->select('*');

        $res = $this->db->get('tbl_moduletype')->result_array();
        return $res;

    }//end allModuleType()


    // Module Section
    public function getUserQuestion($table, $question_type, $user_id)
    {
        $this->db->select('*');
        $this->db->from($table);

        $this->db->where('questionType', $question_type);
        $this->db->where('user_id', $user_id);

        $query = $this->db->get();
        return $query->result_array();

    }//end getUserQuestion()

    public function getModuleQuestion($id,$question_order_id,$status)//id=>module_id
    {
        $this->db->select('*');
        $this->db->from('tbl_modulequestion');        
        $this->db->join('tbl_module','tbl_modulequestion.module_id = tbl_module.id','LEFT');
        $this->db->join('tbl_moduletype','tbl_moduletype.id = tbl_module.moduleType','LEFT');
        $this->db->join('tbl_question','tbl_question.id = tbl_modulequestion.question_id','LEFT');
        $this->db->where('tbl_modulequestion.module_id', $id);
        
        if($status == null)
        {
            $this->db->where('tbl_modulequestion.question_order', $question_order_id);
        }else{
            $this->db->order_by("question_order", "asc");
        }
        
        $query = $this->db->get();
        return $query->result_array();
    }


}//end class
