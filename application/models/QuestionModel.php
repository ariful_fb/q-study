<?php

class QuestionModel extends CI_Model
{

    public $loggedUserId, $loggedUserType;
    function __construct()
    {
        parent::__construct();
        $this->loggedUserId   = $this->session->userdata('user_id');
        $this->loggedUserType = $this->session->userdata('userType');

    }//end __construct()


    /**
     * Update a column with desired value
     *
     * @param  string $tableName    table to get affect
     * @param  string $selector     selector column on which where condition will apply
     * @param  string $value        selected column value
     * @param  array  $dataToUpdate array of data to update ex:['column_name'=>value]
     * 
     * @return null
     */
    public function update($tableName, $selector, $value, $dataToUpdate)
    {
        $dataToUpdate['updated_at'] = time();

        $this->db
        ->where($selector, $value)
        ->update($tableName, $dataToUpdate);

    }//end update()

    /**
     * Delete question info and question module relationship
     * 
     * @param  string $tableName table name to perform operation
     * @param  string $selector  selector column
     * @param  mixed  $value     selector column value
     * 
     * @return void            
     */
    public function delete($tableName, $selector, $value)
    {
        $this->db
        ->where($selector, $value)
        ->delete($tableName);

        return $this->db->affected_rows();
    }


    /**
     * Return question info
     * @param  [type] $questionId [description]
     * @return [type]             [description]
     */
    public function info($questionId)
    {
        $res = $this->db
        ->where('id', $questionId)
        ->get('tbl_question')
        ->result_array();
        
        return count($res[0]) ? $res[0] : [];
    }

    /**
     * Basic insert into database
     * @param  string $tableName    table name
     * @param  array  $dataToInsert data to insert into database
     * @return void                 return void           
     */
    public function insert($tableName, $dataToInsert)
    {
        $this->db
        ->insert($tableName, $dataToInsert);
    }

    /**
     * search from any table using conditions
     * @param  string $tableName table to perform search
     * @param  array  $params    conditions array
     * @return [type]            [description]
     */
    public function search($tableName,$params)
    {
        $res = $this->db
        ->where($params)
        ->get($tableName)
        ->result_array();

        return $res;
    }

}//end class
