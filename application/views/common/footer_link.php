	<!--===================== Footer ========================-->
<footer>
	<div class="container-fluid">
		
		<div class="copyright row">
			<div class="footer_link col-sm-6">
				<ul>
					  
					<li><a href="#">Terms & Conditions</a></li>
					<li><a href="#">Privacy Policy</a></li>
					<li><a href="#">Disclaimer</a></li>
				</ul>
			</div>
			<div class="col-sm-6 text-right">
				<p> Copyright Q-Study &copy; 2014-2015 </p>
			</div>
			
		</div><!--copyright-->
	</div>
</footer>