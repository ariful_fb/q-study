<div class="container">
    <div class="row">
        <!--<div class="col-sm-2"></div>-->
        <div class="col-sm-12">
            <div class="ss_qstudy_list">
                <div class="ss_qstudy_list_top">
                    <form class="form-inline" action="/action_page.php">
                        <div class="form-group">
                            <label for="email">Country</label>
                            <input type="text" class="form-control" readonly="" value="<?php echo $user_info[0]['countryName'];?>">
                        </div>
                        <div class="form-group">
                            <label for="sbjct">Subject</label>
                            <select class="form-control" onchange="getChapter(this)">
                                <option>Select Subject</option>
                                <?php foreach ($subject_info as $subject){?>
                                <option value="<?php echo $subject['subject_id']?>"><?php echo $subject['subject_name']?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="chpter">Chapter</label>
                            <select class="form-control" name="chapter" id="subject_chapter">
                                <option value="">Select Chapter</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">Search</button>
                    </form> 
                </div>
                <div class="ss_qstudy_list_mid">
                    <div class="row">
                        <div class="col-sm-4">
                            <h3 style="text-align: left;">Q-study</h3>
                        </div>
                        <div class="col-sm-4">
                            <h3>Index</h3>
                        </div>
                        <div class="col-sm-4 ss_qstudy_list_mid_right">

                            <div class="profise_techer"><img src="<?php if(isset($all_module[0]['image'])){echo $all_module[0]['image'];}else{?>assets/images/default_user.jpg<?php }?>"></div>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="ss_qstudy_list_bottom tab-pane active" id="all_list" role="tabpanel">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Module Name</th>
                                        <th>Tracker Name</th>
                                        <th>Individual Name</th>
                                        <th>Subject</th>
                                        <th>Chapter</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($all_module as $module){?>
                                    <tr>
                                        <td><a href="get_tutor_tutorial_module/<?php echo $module['id']?>/1"><?php echo $module['moduleName'];?></a></td>
                                        <td><?php echo $module['trackerName'];?></td>
                                        <td><?php echo $module['individualName'];?></td>
                                        <td><?php echo $module['subject_name'];?></td>
                                        <td><?php echo $module['chapterName'];?></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<script>
//$( ".accordion" ).collapse();
function getChapter(e) {
    var subject_id = e.value;
    $.ajax({
        url: "get_chapter_name",
        method: "POST",
        data: {
            subject_id: subject_id
        },
        success: function (response) {
//                $('#add_subject').modal('hide');
console.log(response);
$('#subject_chapter').html(response);
}
});
}
</script>