<form action="" method="post" id="editModuleForm">
  <input type="hidden" name="moduleId" id="moduleId" value="<?php echo $module_info['id']; ?>">
  <div class="container top100">
    <div class="row">

      <div class="col-md-8 upperbutton" style="text-align: right;">
        <div class="blue_photo bottom10">
          <!-- <a onclick="document.getElementById('addModuleForm').submit()" type="submit" >Save</a> -->
          <button class="btn btn-primary" type="submit">Update</button>
        </div>

        <div class="blue_photo bottom10">
          <!-- <a href="">Preview</a> -->
          <button class="btn btn-primary" type="button" id="modulePrevBtn" disabled="true">Preview</button>
        </div>
      </div>
      
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-body" style=" padding: 62px 0; text-align: center; border-top: 43px solid #0663a0; ">
            <b>Successful !!</b>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ok</button>
          </div>
        </div>
      </div>
    </div>
    <div class="row">

      <div class="col-md-4 col-md-offset-4 upperbutton">

      </div>

    </div>

    <div class="row">

      <div class="col-md-2">

        <div class="form-group color_btn top10">
          <label for="exampleInputName2" >Module Name</label>
          <input type="text" class="form-control" name="moduleName" value="<?php echo $module_info['moduleName']; ?>" required>
        </div>

        <div class="form-group color_btn top10">
          <label for="exampleInputName2" >Tracker Name</label>
          <input type="text" class="form-control" name="trackerName" value="<?php echo $module_info['trackerName']; ?>" required>
        </div>

        <div class="form-group color_btn top10">
          <label for="exampleInputName2" >Individual Name</label>
          <input type="text" value="<?php echo $module_info['individualName']; ?>" class="form-control" name="individualName" required>
        </div>

        <div class="form-group color_btn top10">
          <label for="exampleInputName2" >Date & Time</label>
          <div class="form-group color_btn">
            <div class="input-group date" id="datetimepicker1">
              <input type="text" value="<?php echo date('m/d/Y', $module_info['exam_date']); ?>" class="form-control enterDate" id="enterDate" name="dateCreated" autocomplete="off" required>
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>

          <div class="form-check  sms_check top10">
            <label class="form-check-label" for="defaultCheck1">
              Response to sms
            </label>
            <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1" name="isSMS" <?php echo $module_info['isSMS']?' checked' : ''; ?>>
          </div>

          <div class="form-check  sms_check" style=" padding-top: 20px; ">
            <label class="form-check-label" for="defaultCheck1" style=" font-size: 13px; ">
              Assign for all student
            </label>
            <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1" name="isAllStudent" <?php echo $module_info['isAllStudent']?' checked' : ''; ?>>
          </div>

          <div class="form-group color_btn" style=" padding-top: 20px; ">
            <label for="exampleInputEmail2">Assign for individual</label>
            <div class="select">
              <select class="form-control select-hidden allStudents" multiple="multiple" name='individualStudent[]'>
                <?php echo $allStudents; ?>
              </select>

            </div>
          </div>

          <div class="form-check  sms_check" style=" padding-top: 20px; ">
            <label class="form-check-label" for="defaultCheck1" style=" font-size: 13px; ">
              Chose questions
            </label>
            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
          </div>

        </div>
      </div>
      <div class="col-md-10">
        <div class="row">
          <div class="col-md-2">
            <div class="form-group color_btn">
              <label for="exampleInputName2">Country</label>
              <div class="select">
                <select class="form-control select-hidden" name="country" required>
                 <?php echo $all_country; ?>
               </select>
             </div>
           </div>

         </div>

         <div class="col-md-2">
          <div class="form-group color_btn">
            <label for="exampleInputEmail2">Grade/Year/Lavel</label>
            <div class="select">
              <select class="form-control select-hidden" name="studentGrade">
                <?php for($a=1; $a<=13; $a++) : ?>
                  <?php $selStGrade= ($module_info['studentGrade']==$a)?'selected':''; ?>
                  <?php if($a>=13) : ?>
                    <option value="<?php echo $a ?>" <?php echo $selStGrade; ?>>Upper Level</option>
                  <?php else : ?>
                    <option value="<?php echo $a ?>" <?php echo $selStGrade; ?>><?php echo $a; ?></option>
                  <?php endif; ?>
                <?php endfor; ?>
              </select>

            </div>
          </div>

        </div>

        <div class="col-md-2">
          <div class="form-group color_btn">
            <label for="exampleInputEmail2">Module Type</label>
            <div class="select">
              <select class="form-control select-hidden" name="moduleType" required>
                <?php echo $all_module_type; ?>
              </select>
            </div>
          </div>

        </div>

        <div class="col-md-2">
          <div class="form-group color_btn">
            <label for="exampleInputEmail2">Subject</label>
            <div class="select">
              <select class="form-control select-hidden" name="subject" required>
                <?php echo $all_subjects; ?>
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group color_btn">
            <label for="exampleInputEmail2">Chapter</label>
            <div class="select">
              <select class="form-control select-hidden" name='chapter' required>
                <?php echo $all_chapters ?>
              </select>  
            </div>
          </div>
        </div>

        <div class="ss_student_progress">
          <div class="search_filter">
          </div>
        </div>

        <div class="sign_up_menu">
          <div class="table-responsive">
            <?php foreach ($all_question_type as $key) {?>
            <div class="col-md-3">

              <table class="table table-bordered tbl_ques" id="module_setting2">
                <thead>
                  <tr>
                    <th style=""><?php echo $key['questionType'];?><p style="float:right;">Re-Order</p></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1;foreach ($all_question[$key['id']] as $row){?>
                  <tr>
                    
                    <td>
                      <div class="form-check">
                        <label class="form-check-label">
                          
                          <label class="form-check-label" for="defaultCheck21">
                            <input class="form-check-input1" type="checkbox" value="<?php echo $row['id'];?>"  name='moduleQuestion[]' id="quesChecked" <?php echo isset($qoMap[$row['id']])?' checked':''; ?>> Q<?=$i?> 
                            <i class="fa fa-info-circle" style="color:orange;"></i> <i class="fa fa-pencil"></i>
                            
                            <input type="number" min="1" style="max-width: 54px;margin-left: 65px;" autocomplete="off" class='questionOrder' <?php echo !isset($qoMap[$row['id']])?'disabled':'';?>  value="<?php echo isset($qoMap[$row['id']])?$qoMap[$row['id']]:''; ?>" id="qOrdr">
                            
                            <input type="hidden" id="qId_ordr" name="qId_ordr[]" value="<?php echo isset($qoMap[$row['id']])?$row['id'].'_'.$qoMap[$row['id']]:''; ?>">
                            <input type="hidden" id="qId"  value="<?php echo $row['id'];?>">
                          </label>
                        </div>
                      </td>

                    </tr>
                    <?php $i++;}?>

                  </tbody>
                </table>

              </div>
              <?php }?>


            </div>
          </div>


        </div>
      </div>


    </div>
  </div>
</form>
<script>
    //datepicker
    $(document).ready(function(){
     $( "#enterDate" ).on('click', function(){
      $( "#enterDate" ).datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "yyyy/mm/dd",
        orientation: 'bottom',
        onClose: function(){$(this).blur()}
      });
    }) 
   })

    /*select2 on all students*/
    $(document).ready(function() {
      $('.allStudents').select2();
    });

    /*prevent duplicate ordering*/
    $('.questionOrder').on('change', function(e){
        //e.stopPropagation();
        var temp=0;
        var inp = this.value;

        $('.questionOrder').not(this).each(function(){
          if(inp==this.value){
            temp=1;
          }
        });
        
        if(temp){
          alert('Order Overlaping, Please fix');
          $(this).val('');
        }else{
          var qId = $(this).siblings('#qId').val();
          $(this).siblings('input#qId_ordr').val(qId+'_'+inp);
        }

      })

    //checkbox check functionality
    $(document).on('change', '#quesChecked', function(){
      var x = $(this).siblings('#qOrdr');  
      if(this.checked){
        $(this).siblings('#qOrdr').prop('disabled', false);
        $(this).siblings('#qOrdr').prop('required', true);
      }else{
        $(this).siblings('#qOrdr').prop('disabled', true);
        x.val('');
      }
    })

    //module save form submit & preview button enable
    $(document).on('submit', '#editModuleForm', function(e){
      e.preventDefault();
      $.ajax({
        url : 'Module/updateRequestedModule',
        method : 'POST',
        data : $(this).serialize(),
        success : function(data){
          if(data=='true') {
            alert('Module Updated Successfully.');
            //enable preview now
          }else {
            console.log('Something is wrong.');
          } //end else
        } //end success
      });
    })

  </script>