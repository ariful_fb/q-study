<form action="Module/saveModuleQuestion" method="post" id="addModuleForm">
  <input type="hidden" name="startTime" id="modStartTime" value="">
  <input type="hidden" name="endTime" id="modEndTime" value="">
  <input type="hidden" name="optTime" id="modOptTime" value="">
  <div class="container top100">
    <div class="row">

      <div class="col-md-8 upperbutton" style="text-align: right;">
        <div class="blue_photo bottom10">
          <button class="btn btn-primary" type="submit">Save</button>
        </div>

        <div class="blue_photo bottom10">
         <!--  <button class="btn btn-primary" type="button" id="modulePrevBtn" disabled="true">Preview</button> -->
         <a class="btn btn-primary" type="button" id="modulePrevBtn" disabled="true" style="width: 95px;padding: 6px 0px;">Preview</a>
        </div>
      </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-body" style=" padding: 62px 0; text-align: center; border-top: 43px solid #0663a0; ">
            <b>Successful !!</b>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ok</button>
          </div>
        </div>
      </div>
    </div>
    <div class="row">

      <div class="col-md-4 col-md-offset-4 upperbutton">

      </div>

    </div>

    <div class="row">

      <div class="col-md-2">

        <div class="form-group color_btn top10">
          <label for="exampleInputName2" >Module Name</label>
          <input type="text" class="form-control" name="moduleName" required>
        </div>

        <div class="form-group color_btn top10">
          <label for="exampleInputName2" >Tracker Name</label>
          <input type="text" class="form-control" name="trackerName" required>
        </div>

        <div class="form-group color_btn top10">
          <label for="exampleInputName2" >Individual Name</label>
          <input type="text" class="form-control" name="individualName" required>
        </div>

        <div class="form-group color_btn top10">
          <label for="exampleInputName2" >Date</label>
          <div class="form-group color_btn">
            <div class="input-group date" id="datetimepicker1">
              <input type="text" class="form-control enterDate" id="enterDate" name="dateCreated" autocomplete="off">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>
          <div class="form-check">
            <label for="exampleInputName2" >Time</label>
            <i class="fa fa-clock-o" style="font-size:20px;" data-toggle="modal" data-target="#setTime"></i>
          </div>

          <div class="form-check  sms_check top10">
            <label class="form-check-label" for="defaultCheck1">
              Response to sms
            </label>
            <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1" name="isSMS">
          </div>

          <div class="form-check  sms_check" style=" padding-top: 20px; ">
            <label class="form-check-label" for="defaultCheck1" style=" font-size: 13px; ">
              Assign for all student
            </label>
            <input class="form-check-input" type="checkbox" value="1" id="isAllStudent" name="isAllStudent">
          </div>

          <div class="form-group color_btn" style=" padding-top: 20px; ">
            <label for="exampleInputEmail2">Assign for individual</label>
            <div class="select">
              <div style="display: none;" id="hiddenAllStds"><?php echo $allStudents; ?></div>
              <select class="form-control select-hidden allStudents" multiple="multiple" name='individualStudent[]' id="individualStudent">
                <?php echo $allStudents; ?>
              </select>

            </div>
          </div>

          <div class="form-check" style=" padding-top: 20px; ">
            <label class="form-check-label"  for="" style=" font-size: 13px; ">
              Chose questions
            </label>
            <input class="form-check-input chooseQues" type="checkbox" value="" id="chooseQues">
          </div>

        </div>
      </div>
      <div class="col-md-10">
        <div class="row">
          <div class="col-md-2">
            <div class="form-group color_btn">
              <label for="exampleInputName2">Country</label>
              <div class="select">
                <select class="form-control select-hidden" name="country" required>
                 <?php echo $all_country; ?>
               </select>
             </div>
           </div>

         </div>

         <div class="col-md-2">
          <div class="form-group color_btn">
            <label for="exampleInputEmail2">Grade/Year/Lavel</label>
            <div class="select">
              <select class="form-control select-hidden" id="studentGrade" name="studentGrade" required>
                <?php for($a=1; $a<=13; $a++) : ?>
                  <?php if($a>=13) : ?>
                    <option value="<?php echo $a ?>">Upper Level</option>
                  <?php else : ?>
                    <option value="<?php echo $a ?>"><?php echo $a; ?></option>
                  <?php endif; ?>
                <?php endfor; ?>
              </select>

            </div>
          </div>

        </div>

        <div class="col-md-2">
          <div class="form-group color_btn">
            <label for="exampleInputEmail2">Module Type</label>
            <div class="select">
              <select class="form-control select-hidden" name="moduleType" required>
                <?php echo $all_module_type; ?>
              </select>
            </div>
          </div>

        </div>

        <div class="col-md-2">
          <div class="form-group color_btn">
            <label for="exampleInputEmail2">Subject</label>
            <div class="select">
              <select class="form-control select-hidden" name="subject" id="subject" required>
                <?php echo $all_subjects; ?>
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group color_btn">
            <label for="exampleInputEmail2">Chapter</label>
            <div class="select">
              <select class="form-control select-hidden" name='chapter'  id="chapter">
                <?php echo $all_chapters ?>
              </select>  
            </div>
          </div>
        </div>

        <div class="ss_student_progress">
          <div class="search_filter">
          </div>
        </div>

        <div class="sign_up_menu"> 
          <div class="table-responsive" id="allQuestion"></div>
          <!-- <div class="table-responsive">
            <?php foreach ($all_question_type as $key) {?>
            <div class="col-md-3">
                  
              <table class="table table-bordered tbl_ques" id="module_setting2">
                <thead>
                  <tr>
                  
                    <th style=""><?php echo $key['questionType'];?><p style="float:right;">Re-Order</p></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1;foreach ($all_question[$key['id']] as $row){?>
                  <tr>
                  
                    <td>
                      <div class="form-check">
                        <label class="form-check-label">
                  
                          <label class="form-check-label" for="defaultCheck21">
                            <input class="form-check-input1" type="checkbox" value="<?php echo $row['id'];?>"  name='moduleQuestion[]' id="quesChecked"> Q<?=$i?> 
                            <i class="fa fa-info-circle" style="color:orange;"></i> <i class="fa fa-pencil"></i>
                  
                            name="questionOrder[]"      
                            <input type="number" min="1" style="max-width: 54px;margin-left: 65px;" autocomplete="off" class='questionOrder' disabled="disabled" value="" id="qOrdr">
                            <input type="hidden" id="qId_ordr" name="qId_ordr[]" value="">
                            <input type="hidden" id="qId"  value="<?php echo $row['id'];?>">
                          </label>
                        </div>
                      </td>
                  
                    </tr>
                    <?php $i++;}?>
                  
                  </tbody>
                </table>
                  
              </div>
              <?php }?>
                  
                  
            </div> --></div>


        </div>
      </div>


    </div>
  </div>
</form>

<!--modal add time optional and specific-->
<div class="modal fade" id="setTime" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Set Time</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group row">
            <label for="recipient-name" class="control-label col-md-3">Time Range:</label>
            <div class="col-md-3">
              <div class="input-group date" id="datetimepicker1">
                <input type="text" class="form-control enterDate" id="timeStart" name="dateCreated" autocomplete="off">
                <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
                </span>
              </div>
            </div>
            <label  class="control-label small text-muted col-md-1">To</label>
            <div class="col-md-3">
              <div class="input-group date" id="datetimepicker1">
                <input type="text" class="form-control enterDate" id="timeEnd" name="dateCreated" autocomplete="off">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="recipient-name" class="control-label col-md-3">Optional Time:</label>
            <div class="col-md-4">
              <div class="input-group date" id="datetimepicker1">
                <input type="text" class="form-control enterDate" id="optTime" name="dateCreated" autocomplete="off">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="modTimeSetBtn">OK</button>
      </div>
    </div>
  </div>
</div>


<script>
    //datepicker
    $(document).ready(function(){
     $( "#enterDate" ).on('click', function(){
      $( "#enterDate" ).datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "yyyy/mm/dd",
        orientation: 'bottom',
        onClose: function(){$(this).blur()}
      });
    }) 
   });
    
      //time picker
      var startTimeTextBox = $('#timeStart');
      var endTimeTextBox = $('#timeEnd');
      $.timepicker.timeRange(
        startTimeTextBox,
        endTimeTextBox,
        {
        minInterval: (1000*60), // 1hr
        //timeFormat: 'HH:mm',
        timeFormat: 'hh:mm tt z',
        start: {}, // start picker options
        end: {} // end picker options
      },
      );

      $('#optTime').timepicker();
      //set hidden field on time set
      $(document).on('click', '#modTimeSetBtn', function () {
        var timeStart = $('#timeStart').val();
        var timeEnd = $('#timeEnd').val();
        var optTime = $('#optTime').val();
        $('#modStartTime').val(timeStart);
        $('#modEndTime').val(timeEnd);
        $('#modOptTime').val(optTime);
        console.log(timeStart);
        $('#setTime').modal('toggle');
      })


      /*select2 on all students*/
      $(document).ready(function() {
        $('.allStudents').select2();
      });

      /*prevent duplicate ordering*/
      $(document).ready(function(){
        $(document).on('change', '.questionOrder', function(e){
          //e.stopPropagation();
          var temp=0;
          var inp = this.value;

          $('.questionOrder').not(this).each(function(){
            if(inp==this.value){
              temp=1;
            }
          });
          
          if(temp){
            alert('Order Overlaping, Please fix');
            $(this).val('');
          }else{
            var qId = $(this).siblings('#qId').val();
            $(this).siblings('input#qId_ordr').val(qId+'_'+inp);
          }

        });


        //question checkbox check functionality
        $(document).on('change', '#quesChecked', function(){
           console.log('hit');
          var x = $(this).siblings('#qOrdr');  
          if(this.checked){
            $(this).siblings('#qOrdr').prop('disabled', false);
            $(this).siblings('#qOrdr').prop('required', true);
          }else{
            $(this).siblings('#qOrdr').prop('disabled', true);
            x.val('');
          }
        });

    })


    //module save form submit & preview button enable
    $(document).on('submit', '#addModuleForm', function(e){
      e.preventDefault();
      var pathname = '<?php echo base_url(); ?>';
      $.ajax({
        url : 'Module/saveModuleQuestion',
        method : 'POST',
        data : $(this).serialize(),
        success : function(data){
          if(data=='true') {
            alert('Module added successfully.');
            $('#modulePrevBtn').attr('disabled', false);
            $("#modulePrevBtn").attr("href", pathname+'module_preview/'+data+'/1');
          }else {
            console.log('Something is wrong.');
          } //end else
        } //end success
      });
    });

    /*get all question by search params on check choose question*/
    $(document).on('change', '.chooseQues', function(){
      if(this.checked){
        var studentGrade = $("#studentGrade :selected").val();
        var subject = $("#subject :selected").val();
        var chapter = $("#chapter :selected").val();
        $.ajax({
          url : 'Module/quesSearch',
          method :'POST',
          data : {studentGrade:studentGrade, subject:subject, chapter:chapter},
          success : function(data){
            $('#allQuestion').html(data);
          } 
        })

      }else{
        $('#allQuestion').html('');
      }
    });

    /*assign all student checkbox check*/
    $(document).on('change', '#isAllStudent', function(){
      if(this.checked){
        $('#individualStudent :selected').removeAttr("selected");
        $('#individualStudent').html('');
        $('#individualStudent').attr('disabled', true);
      }else{
         $('#individualStudent').attr('disabled', false);
         $($('#individualStudent')).html($('#hiddenAllStds').html());
      }
    })

  </script>