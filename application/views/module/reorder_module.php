    
<div class="container top100">
    <div class="row">

        <div class="col-md-10 text-center">
            <?php if($this->session->userdata('success_msg')) : ?>
                <div class="alert alert-success alert-dismissible show" role="alert">
                    <strong><?php echo $this->session->userdata('success_msg'); ?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php elseif($this->session->userdata('error_msg')) : ?>
                <div class="alert alert-danger alert-dismissible show" role="alert">
                    <strong><?php echo $this->session->userdata('error_msg'); ?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
        </div>
    </div> <!-- end row -->
    <form action="" method="post" id="reorderModuleForm">
        <div class="row text-center">
            <button class="btn btn-primary" type="submit">Save</button>
            <button class="btn btn-primary" type="button" disabled="true">Preview</button>
        </div> <!-- end save preview btn row -->

        <div class="row">
        <br>
            <div class="col-md-4"></div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="exampleInputName2">Country</label>
                    <div class="select">
                        <select class="form-control" name="" id="">
                            <?php echo $all_country; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                
                <div class="form-group">
                        <label for="exampleInputEmail2">Module Type</label>
                        <div class="select">
                            <select class="form-control select-hidden">
                                <option>Select....</option>
                                <?php foreach ($all_module_type as $module_type){?>
                                <option value="<?php echo $module_type['id']?>">
                                    <?php echo $module_type['module_type'];?>
                                </option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                <br>
                    <button type="submit" class="btn btn_green"><i class="fa fa-search"></i>Choose Module</button>
                </div>
            </div>

            <div class="col-md-4">
            <!-- <div class=" ss_student_progress">
             
                 <div class="search_filter">
             
                     
             
                     
             
             
                     <div class="form-group">
                         <button type="submit" class="btn btn_green"><i class="fa fa-search"></i>Choose Module</button>
                     </div>
                 </div>
             </div> --> <!-- end .ss_student_progress -->


            </div>
            

        </div> <!-- row -->

        <div class="sign_up_menu">
            <div class="table-responsive">
                <table class="table table-bordered" id="module_setting">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Module Name</th>
                            <th>Module Type</th>
                            <th>Subject</th>
                            <th>Reorder</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $row; ?>

                    </tbody>
                </table>
            </div>
        </div><!-- end .sign_up_menu -->
    </form>
</div>


<script>
    /*prevent duplicate ordering for module*/
    $('.moduleOrder').on('change', function(e){
        //e.stopPropagation();
        var temp=0;
        var inp = this.value;

        $('.moduleOrder').not(this).each(function(){
            if(inp==this.value){
                temp=1;
            }
        });
        
        if(temp){
            alert('Order Overlaping, Please fix');
            $(this).val('');
        }else{
            var modId = $(this).siblings('#modId').val();
            $(this).siblings('input#modId_ordr').val(modId+'_'+inp);
        }

    })

    //checkbox check functionality on module ordering
    $(document).on('change', '#moduleChecked', function(){
          var x = $(this).siblings('#modOrdr');  
          if(this.checked){
            $(this).siblings('#modOrdr').prop('disabled', false);
            $(this).siblings('#modOrdr').prop('required', true);
        }else{
            $(this).siblings('#modOrdr').prop('disabled', true);
            x.val('');
        }
    })

    //module ordering form submit & preview button enable
    $(document).on('submit', '#reorderModuleForm', function(e){
          e.preventDefault();
          $.ajax({
            url : 'Module/saveModuleOrdering',
            method : 'POST',
            data : $(this).serialize(),
            success : function(data){
              if(data=='true') {
                alert('Module Order Updated Successfully.');
              }else {
                alert('Something is wrong.');
              } //end else
            } //end success
        });
    })

</script>
