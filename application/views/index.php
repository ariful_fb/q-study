<?php echo $header; ?>
<?php echo $menu; ?>
<section class="main_content bg-gray animatedParent">
		<div class="container-fluid container-fluid_padding">
			<div class="row">
			
			<div class="col-md-3">
				<div class="left_menu1 bottom10">
					<ul id="counter">
						<li>Become a best student...</li>
						<li>Recent concept and technology</li>
						<li>Quick method of memorization</li>
						<li>Latest Innovation project of mathematics</li>
					</ul>
				</div>

			<div class="left_banner2 bottom10 ss_l_img">
			<img class="img-responsive "  src="assets/images/2.png">
			</div>
			
			
			</div>
			
			<div class="col-md-6">
			  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
   

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="<?php echo base_url();?>assets/images/banner/1.png" alt="Los Angeles" style="width:100%;">
      </div>

      <div class="item">
        <img src="<?php echo base_url();?>assets/images/banner/2.jpg" alt="Chicago" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="<?php echo base_url();?>assets/images/banner/3.jpg" alt="New york" style="width:100%;">
      </div>
      <div class="item">
        <img src="<?php echo base_url();?>assets/images/banner/4.jpg" alt="New york" style="width:100%;">
      </div>
      <div class="item">
        <img src="<?php echo base_url();?>assets/images/banner/5.jpg" alt="New york" style="width:100%;">
      </div>
      <div class="item">
        <img src="<?php echo base_url();?>assets/images/banner/6.jpg" alt="New york" style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
			</div>
			
			
			<div class="col-md-3">
				<div class="tutor bottom10">
				<div class="text-right">
					<img class="text-right" src="<?php echo base_url();?>assets/images/pp.jpg">
					</div>
					<h6><b>Become a tutor...</b></h6>
					<a class="a_button text-center" href="">Click Here</a>
				</div>
				<div class="ss_l_img">
				<img class="img-responsive "  src="<?php echo base_url();?>assets/images/3.jpg"></div>
			</div>
			
			</div>
			
			<div class="row ss_home_bottom">
			<div class="col-md-3 bottom10">
			<img class="img-responsive "  src="<?php echo base_url();?>assets/images/left2.png">
			</div>
			
			<div class="col-md-6 bottom10">
				<img class="img-responsive"  src="<?php echo base_url();?>assets/images/m.bottom.png">
			</div>
			
			<div class="col-md-3 bottom10">
			<img class="img-responsive "  src="<?php echo base_url();?>assets/images/5.jpg">
			</div>
			
			</div>
			
			
			
			
			
			<!--===================== End of Hosting Software ========================-->
		</div>
	</section>
	<!--===================== End of Why Choose ========================-->
<?php echo $footer_link; ?>
<?php echo $footer; ?>