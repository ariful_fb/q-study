<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//public $ci =& get_instance();

/**
     * Before passing items to renderSkpQuizPrevTable() index it first with this func
     * basically for preview skip quiz table
     *
     * @param array $items Json object array.
     *
     * @return array        Array with proper indexing
     */
    function indexQuesAns(stdClass $items)
    {
     
        $arr = [];
        
        foreach ($items as $item) {
            $temp            = json_decode($item);
            $cr              = explode('_', $temp->cr);
            $col             = $cr[0];
            $row             = $cr[1];
            $arr[$col][$row] = [
                'type' => $temp->type,
                'val'  => $temp->val,
            ];
        }

        return $arr;

    }//end indexQuesAns()


    /**
     * Render the indexed item to table data for preview
     *
     * @param  array   $items   ques ans as indexed item(get processed items from indexQuesAns())
     * @param  integer $rows    num of row in table
     * @param  integer $cols    num of cols in table
     * @param  integer $showAns optional, set 1 will show the answers too
     * @return string           table item
     */
    function renderSkpQuizPrevTable($items, $rows, $cols, $showAns=0, $pageType='')
    {
        // print_r($items);die;
        $row = '';
        for ($i = 1; $i <= $rows; $i++) {
            $row .= '<tr>';
            for ($j = 1; $j <= $cols; $j++) {
                if ($items[$i][$j]['type'] == 'q') {
                    $row .= '<td><input type="button" data_q_type="0" data_num_colofrow="" value="'.$items[$i][$j]['val'].'" name="skip_counting[]" class="form-control rsskpin input-box  rsskpinpt'.$i.'_'.$j.'" readonly style="min-width:50px; max-width:50px; background-color:#ffb7c5;">';
                    if ($pageType = 'edit') {
                        $quesObj = [
                            'cr'   => $j.'_'.$i,
                            'val'  => $items[$i][$j]['val'],
                            'type' => 'q',
                        ];
                        $quesObj = json_encode($quesObj);
                        $row    .= '<input type="hidden" value=\''.$quesObj.'\' name="ques_ans[]" id="obj">';
                        // $row .= '<input type="hidden" value=\''.$quesObj.'\' name="ans[]" id="ans_obj">';
                    }

                    $row .= '</td>';
                } else {
                    $ansObj = [
                        'cr'   => $i.'_'.$j,
                        'val'  => $items[$i][$j]['val'],
                        'type' => 'a',
                    ];
                    $ansObj = json_encode($ansObj);
                    $val    = ($showAns == 1) ? ' value="'.$items[$i][$j]['val'].'"' : '';

                    $row .= '<td><input autocomplete="off" type="text" '.$val.' data_q_type="0" data_num_colofrow="'.$i.'_'.$j.'" value="" name="skip_counting[]" class="form-control rsskpin input-box ans_input  rsskpinpt'.$i.'_'.$j.'"  style="min-width:50px; max-width:50px;background-color:#baffba;">';
                    $row .= '<input type="hidden" value="" name="given_ans[]" id="given_ans">';
                    if ($pageType = 'edit') {
                        $row .= '<input type="hidden" value="" name="ques_ans[]" id="obj">';
                        $row .= '<input type="hidden" value=\''.$ansObj.'\' name="ans[]" id="ans_obj">';
                    }

                    $row .= '</td>';
                }//end if
            }//end for

            $row .= '</tr>';
        }//end for

        return $row;

    }//end renderSkpQuizPrevTable()


    /**
     * Make table row element with assignment tasks
     *
     * @param  array $items assignment tasks json array
     * @return string        table row element
     */
    function renderAssignmentTasks(array $items, $pageType='')
    {
        $row = '';
        foreach ($items as $task) {
            $task     = json_decode($task);
            $qMark    = $task->qMark;
            $obtnMark = $task->obtnMark;
            if ($pageType == 'edit') {
                $qMark    = '<input name="qMark[]" class="form-control" type="text" value="'.$qMark.'"'.' type="number" step="0.1" required>';
                $obtnMark = '<input name="obtnMark[]" class="form-control" type="text" value="'.$obtnMark.'"'.'type="number" step="0.1" required>';
            }

            $row .= '<tr id="'.($task->serial + 1).'">';
            $row .= '<td>'.($task->serial + 1).'</td>';
            $row .= '<td>'.$qMark.'</td>';
            $row .= '<td>'.$obtnMark.'</td>';
            $row .= '<td><i class="fa fa-eye qDtlsOpenModIcon" data-toggle="modal" data-target="#quesDtlsModal"></i></td>';
            $row .= '<input name="descriptions[]" type="hidden" id="hiddenTaskDesc" value="'.$task->description.'">';
            $row .= '</tr>';
        }

        return $row;

    }//end renderAssignmentTasks()